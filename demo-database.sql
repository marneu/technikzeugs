-- MariaDB dump 10.19  Distrib 10.6.4-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: technikzeugs
-- ------------------------------------------------------
-- Server version	10.6.4-MariaDB-1:10.6.4+maria~focal

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `artikel`
--

DROP TABLE IF EXISTS `artikel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `artikel` (
  `a-id` int(11) NOT NULL AUTO_INCREMENT,
  `titel` varchar(512) NOT NULL,
  `text` varchar(10000) NOT NULL,
  `tags` varchar(256) NOT NULL,
  `datum` date NOT NULL,
  `bild` varchar(128) NOT NULL,
  `owner_id` int(11) NOT NULL,
  PRIMARY KEY (`a-id`),
  KEY `artikel_owner_id_8cc0bb5e_fk_auth_user_id` (`owner_id`)
) ENGINE=InnoDB AUTO_INCREMENT=435 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `artikel`
--

LOCK TABLES `artikel` WRITE;
/*!40000 ALTER TABLE `artikel` DISABLE KEYS */;
INSERT INTO `artikel` VALUES (332,'Ubuntu LTS hardware enablement stack: updates for kernel and drivers for ubuntu LTS','https://wiki.ubuntu.com/Kernel/LTSEnablementStack\r\n\r\nfor kernel 4.8:\r\n    sudo apt-get install --install-recommends linux-generic-hwe-16.04 xserver-xorg-hwe-16.04  \r\n\r\nfor some bleeding-edge-y-ness aka kernel 4.10:\r\n    sudo apt-get install --install-recommends linux-generic-hwe-16.04-edge xserver-xorg-hwe-16.04  \r\n','ubuntu, kernel, hwe','2017-04-17','',2),(333,'TP-Link USB-WiFi Stick','So installiert man TL-WN823N v2 unter Debian Linux:\r\n\r\nsudo apt install linux-headers-amd64 firmware-realtek\r\ngit clone https://github.com/Mange/rtl8192eu-linux-driver\r\ncd rtl8192eu-linux-driver\r\nmake\r\nsudo make install\r\nsudo modprobe 8192eu\r\n\r\n','debian, wifi, tp-link, driver, wlan','2017-05-21','',2),(334,'XKCD-password generator in python','python lernen mit web-IDE:\r\nhttp://interactivepython.org/runestone/static/everyday/2013/01/4_password.html\r\n\r\n\r\n','python, password','2017-04-28','',2),(336,'markdown','markdown-referenz: markdown.de/ \r\n\r\nmarkdown-dokumente in PDF umwandeln(kann nicht mit html-tabellen umgehen): \r\nwww.markdowntopdf.com/ \r\n\r\nmarkdown-dokumente in PDF umwandeln(mit html-tabellen und github-css):\r\ngitprint.com/ \r\n\r\nmarkdown-dokumente in PDF umwandeln(Atom-plugin): \r\natom.io/packages/markdown-themeable-pdf \r\n\r\ncommandline-tool für allerlei formate: pandoc (für pdf ist \'pdflatex\'-plugin notwendig) \r\npandoc.org/MANUAL.html \r\n\r\n    pandoc GA1_misc.md -f  markdown_strict -s -o GA1_misc.html \r\n\r\n','markdown, pandoc, markup','2017-05-04','',2),(337,'reStructuredText','quickref\r\nhttp://docutils.sourceforge.net/docs/user/rst/quickref.html\r\n\r\n.. raw:: html\r\nhttp://docutils.sourceforge.net/docs/ref/rst/directives.html#raw-data-pass-through\r\n\r\nblock quotes\r\nhttp://docutils.sourceforge.net/docs/ref/rst/restructuredtext.html#block-quotes\r\n\r\ncode\r\nhttp://docutils.sourceforge.net/docs/ref/rst/directives.html#code\r\n\r\nDokumentationsgenerierung mit Sphinx und rst:\r\nhttp://www.sphinx-doc.org/en/stable/tutorial.html\r\n\r\n','restructuredtext, rst, markup','2017-06-21','',2),(338,'apt/dpkg show installed packages','dpkg-query -Wf \'${Installed-Size}\\t${Package}\\t${Priority}\\n\' | sort -nr | more\r\n\r\n# list installed packages, can be dumped into file \'packages-list\'\r\ndpkg --get-selections >packages-list\r\n\r\n\r\n\r\n','apt, dpkg, debian','2017-05-14','',2),(339,'debian wi-fi without network-manager','xfce4 deinstalliert, muss jetzt wi-fi ohne network-manager konfigurieren:\r\n\r\nVerbindungs-Info kopieren:\r\n/etc/NetworkManager/system-connections/[wlan-name]\r\n\r\ninfos einfügen hier:\r\n/etc/network/interfaces\r\n\r\nauto wlan0\r\niface wlan0 inet dhcp\r\n        wpa-ssid myssid\r\n        wpa-psk my_wlan_password\r\n\r\n#iface wlan1 inet static\r\n#wpa-roam /etc/wpa_supplicant/wpa_supplicant.conf\r\n\r\niface wlan1 inet static\r\n    wpa-ssid myssid\r\n    wpa-psk my_wlan_password\r\n    address 192.168.2.88\r\n    netmask 255.255.255.0\r\n    network 192.168.2.0\r\n    gateway 192.168.2.1\r\n\r\n\r\nund jetzt hochbringen! \r\n     sudo ifdown wlan1\r\n     sudo ifup wlan1\r\n\r\n-> https://wiki.debian.org/WiFi/HowToUse#wpa_supplicant\r\n\r\n','debian, wi-fi, wlan','2017-05-14','',2),(340,'Netzwerkkarte heraussfinden','lspci -v | grep \"Network controller\"\r\n\r\n\r\n\r\n','debian, network','2017-05-17','',2),(341,'set appearance via file with xsettingsd','print current settings:\r\n    \r\n    dump_settings\r\n\r\nstart daemon(needs to be restarted to show changes in config file):\r\n    \r\n    xsettingsd &\r\n\r\nset fontsize:\r\n     Gtk/FontName \"Ubuntu 10.5\"\r\n\r\nwebsite:\r\nhttps://github.com/derat/xsettingsd/wiki/Installation\r\n\r\nexample with more options written out:\r\nhttps://gist.github.com/benley/56cae72f151b972db63f\r\n\r\n','linux, rice, xsettingsd','2017-12-24','',2),(342,'lxc-container mit lxd','installation:\r\nhttps://stgraber.org/2016/03/15/lxd-2-0-installing-and-configuring-lxd-212/\r\n\r\ninstallation + usage:\r\nhttps://linuxcontainers.org/lxd/getting-started-cli/\r\n\r\nGUI-apps mit audio (gui funzt, audio nicht):\r\nhttps://blog.simos.info/how-to-run-graphics-accelerated-gui-apps-in-lxd-containers-on-your-ubuntu-desktop/\r\n\r\nkönnte audio-problem lösen:\r\nhttps://bigjools.wordpress.com/2016/07/13/webex-using-ubuntu-lxd-containers/\r\n\r\n','ubuntu, lxd, lxc, container','2017-05-19','',2),(343,'docker: GUI-apps','firefox:\r\nhttp://fabiorehm.com/blog/2014/09/11/running-gui-apps-with-docker/\r\n\r\nalles andere:\r\nhttps://blog.jessfraz.com/post/docker-containers-on-the-desktop/\r\n\r\n','docker, container','2017-05-19','',2),(344,'proxy-webseite','mit dieser proxy-webseite lässt sich geoblocking umgehen - fick deutsche zensur und GEMA-querelen:\r\n\r\nhttps://us.hideproxy.me/index.php\r\n\r\n\r\n','proxy, privacy, censorship, geoblocking','2017-07-19','',2),(346,'connect windows host to LAN server (minecraft)','Network & Internet -> Ethernet -> Change adapter options -> rightclick Ethernet -> properties -> rightclick Internet Protocol Version 4 (TCP/IPv4)-> properties\r\n\r\nhttps://idoc.vsb.cz/xwiki/wiki/infra/view/tuonet/tcp-ip-win10?language=en\r\n\r\n\r\n','windows, minecraft','2017-07-29','',2),(347,'merging and splitting pdf files','There are several ways and applications to do this, I used this one just now:\r\n\r\n   sudo apt install pdftk\r\n\r\n   pdftk arbeitsvertrag.pdf burst\r\n\r\n   pdftk pg_0001.pdf pg_0002.pdf pg_0003.pdf pg_0004.pdf pg_0005.pdf \\\r\n    unterschrift.pdf output arbeitsvertrag_unterschrieben.pdf \r\n\r\nhowto pdftk:\r\nhttp://linuxcommando.blogspot.de/2013/02/splitting-up-is-easy-for-pdf-file.html\r\nhowto convert(imagemagick):\r\nhttp://linuxcommando.blogspot.de/2015/03/how-to-merge-or-split-pdf-files-using.html\r\nstack exchange thread:\r\nhttps://askubuntu.com/questions/221962/how-can-i-extract-a-page-range-a-part-of-a-pdf\r\n\r\n\r\n','pdf, linux','2017-08-10','',2),(348,'apt list installed applications','$ apt list --installed | grep firefox\r\n\r\nWARNING: apt does not have a stable CLI interface. Use with caution in scripts.\r\n\r\nfirefox/sonya,now 55.0.2+linuxmint1+sonya amd64 [installed]\r\nfirefox-locale-en/sonya,now 55.0.2+linuxmint1+sonya amd64 [installed]\r\n\r\n\r\n','apt','2017-08-21','',2),(351,'Firefox show generated HTML','Generated HTML anzeigen, d.h. das, was aus JavaScript + HTML erzeugt wird:\r\n\r\nStrg + A (alles auswählen)\r\nRechtsklick -> View Source\r\n\r\n','firefox, html, js','2017-08-23','',2),(352,'prevent screen from turning off with xset','how to prevent the screen from turning off under linux mint:\r\n\r\ncommands:\r\n\r\n    xset s off\r\n    xset -dpms\r\n    xset s noblank\r\n\r\nzum kopieren:\r\n\r\nxset s off && xset -dpms && xset s noblank\r\n\r\n\r\n\r\n','screensaver, power, xset','2017-11-07','',2),(353,'minimal install with i3wm on server OS','to get the script over:\r\n\r\n  $ rsync -r /media/rynnon/DATAPUPPY/_marneu/rice/* mn@192.168.2.120:/home/mn/\r\n\r\nUbuntu Server 16.04:\r\n\r\n  $ sudo apt install xorg i3-wm\r\n\r\nmisc. CLI:\r\n\r\n  $ sudo apt install htop mlocate nano ranger openssh-server\r\n\r\n  $ sudo apt remove lxc lxc-common lxd lxd-client ftp snapd ed\r\n\r\n   sudo add-apt-repository ppa:dawidd0811/neofetch -y\r\n\r\ndev:\r\n  sudo apt install git python3-pip mysql-client mysql-server\r\n\r\nlightdm ist eine gute Sache, benötigt aber 314 dependencies. dann lieber mit \"startx\" starten und manuell .Xresources mit \"xrdb .Xresources\" laden.\r\n\r\n','i3','2017-08-31','',2),(354,'uwsgi + django + ngninx ressourcen','Offizielle uWSGI (\"micro-wisgi\") doku:\r\n  Quickstart:\r\n  http://uwsgi-docs.readthedocs.io/en/latest/WSGIquickstart.html\r\n  Django + nginx:\r\n  http://uwsgi-docs.readthedocs.io/en/latest/tutorials/Django_and_nginx.html\r\n\r\nDjango-docs für deployment mit wsgi:\r\nhttps://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/\r\n\r\nStackoverflow (Pfadprobleme, Beispiel-Configs):\r\nhttps://stackoverflow.com/questions/9927604/why-my-uwsgi-cannot-find-my-django-application\r\nhttps://stackoverflow.com/questions/37647730/set-path-to-uwsgi-correctly\r\n\r\nnginx-logs:\r\n\r\n   sudo tail -n 20 -f /var/log/nginx/access.log\r\n   sudo tail -n 20 -f /var/log/nginx/error.log\r\n\r\n->http://linuxcommando.blogspot.de/2007/11/log-watching-using-tail-or-less.html (nicht ganz aktuell, aber kommentare helfen)\r\n\r\nBefehle:\r\n   # nur wenn es auf dem System noch nie gemacht wurde:\r\n   sudo ln -s ~/dev/system_automation/system_automation_nginx_marlon.conf /etc/nginx/sites-enabled/\r\n\r\n   source ~/.virtualenvs/system_automation/bin/activate\r\n   cd ~/dev/system_automation\r\n   uwsgi --socket :8002 --module system_automation.wsgi\r\n   sudo systemctl restart nginx.service\r\n\r\n','wsgi, django, nginx','2017-08-31','',2),(356,'python: list comprehensions, map, filter, reduce - functional stuff','map, filter and reduce: http://book.pythontips.com/en/latest/map_filter.html\r\n\r\nlist comprehensions: http://www.secnetix.de/olli/Python/list_comprehensions.hawk\r\n\r\nprinting a map object: https://stackoverflow.com/questions/1303347/getting-a-map-to-return-a-list-in-python-3-x\r\n\r\nlist comprehensions vs. generators: https://medium.freecodecamp.org/python-list-comprehensions-vs-generator-expressions-cef70ccb49db\r\n\r\nfunctional programming in python: https://maryrosecook.com/blog/post/a-practical-introduction-to-functional-programming\r\n\r\n\r\n\r\n','python, map, filter, reduce, functional, list-comprehension','2017-09-02','',2),(357,'declarative (and functional) programming in javascript','pretty good overview on what declarative programming is, what the benefits are, and how it\'s done:\r\nhttps://tylermcginnis.com/imperative-vs-declarative-programming/\r\n\r\n\r\n','javascript, declarative','2017-09-02','',2),(358,'find','wie man das cli-tool find (standard unter ubuntu) verwendet: http://www.omgubuntu.co.uk/2017/09/finding-files-in-command-line\r\n\r\n   $ sudo find / -type f -mmin -10\r\n\r\nThis example will find (starting at the root directory, or /, and recursively search subdirectories) all normal files (-type f means normal files, without this it will find normal files + special files + directories) which were modified less than ten minutes ago (-mmin -10), and then display the results for you.\r\n\r\n','cli, find','2017-09-02','',2),(359,'convert ttf-fonts to woff2-fonts (aka webfonts) and back','needs manual compilation, but whatever: https://github.com/google/woff2\r\n\r\n','woff2, font','2017-09-02','',2),(360,'adding new fonts to system','per user and per system: https://community.linuxmint.com/tutorial/view/29\r\n\r\nreload fontcache:\r\n\r\n   (sudo) fc-cache -fv\r\n\r\n\r\n\r\n','font','2017-09-02','',2),(361,'article: how the web became unreadable','https://www.wired.com/2016/10/how-the-web-became-unreadable/\r\n\r\neines der besten zitate:\r\n\r\n\"Apple’s typography guidelines suggest that developers aim for a 7:1 contrast ratio. But what ratio, you might ask, is the text used to state the guideline? It’s 5.5:1.\r\n\r\n','font, human-interface, readability, accessibility','2017-09-02','',2),(362,'Hex to RGB converter','a neat and minimalistic Converter for HEX-Values: https://www.webpagefx.com/web-design/hex-to-rgb/\r\n\r\n','hex, rgb, color','2017-09-02','',2),(363,'the definite answer to \"Django vs. Rails\"','http://hackwrite.com/posts/python-and-django-vs-ruby-and-rails/\r\n\r\n\"So, who uses Python? Nerds, mostly. Sciencey people. Math people. People who care about data. People who know three or four or a dozen other programming languages. People who prefer PostgreSQL to MongoDB, because they care about things like data integrity. People who actually understand what polymorphism is. People who want to teach computers to write music or recognize handwriting.\r\n\r\nBecause these are the kind of people who have gotten into Python, the tools exist in Python to do that kind of work. And because those tools exist, more and more of those kinds of people have gravitated to the language, generating a positive feedback loop and a network effect of geeky awesomeness.\r\n\r\nAnd who uses Ruby? People who want to get rich building the next Twitter.\"\r\n\r\n','django, rails, python, ruby','2017-09-04','',2),(364,'show hardware information','https://unix.stackexchange.com/questions/75750/how-can-i-find-the-hardware-model-in-linux\r\n\r\n$ sudo dmidecode | grep -A3 \'^System Information\'\r\n[sudo] password for marlon: \r\nSystem Information\r\n	Manufacturer: LENOVO\r\n	Product Name: 20FH002RGE\r\n	Version: ThinkPad T560\r\n\r\n\r\n','hardware, cli','2017-09-12','',2),(365,'old (EOL) versions of linux','Download old (incl. post-EOL) versions of linux distros:\r\nhttps://old-linux.com/\r\n\r\nThis is the only source I found for Mint 5 (based on Ubuntu 8.04). Ubuntu itself is more readily available, but does not include software such as Flash player out of the box, which is necessary for a lot of the Internet back then (e.g. Homestuck). The package manager is kind of useless for EOL distros, since all the repository mirrors are down. \r\n\r\nSee this answer on how to add repository mirrors for EOL ubuntu releases: https://superuser.com/a/339572 \r\n\r\n','linux, old, eol','2017-09-15','',2),(367,'Smartphone with Lineage','------------ LG G5 -------------------------\r\n\r\n- ca. 400 € (300 gebraucht)\r\n- wechselbarer akku\r\n- 5,3\" display\r\n- lineage os \r\n\r\nROM: https://download.lineageos.org/h850\r\n\r\nreview: http://www.expertreviews.co.uk/lg/lg-g5\r\n\r\nreview: http://www.techradar.com/reviews/phones/mobile-phones/lg-g5-1315187/review\r\n\r\ninstall: https://www.getdroidtips.com/lineage-os-14-1-lg-g5/\r\n\r\ninstall: http://rootmygalaxy.net/lineageos-15-0-for-lg-g5/\r\n\r\nhow to enter recovery mode: http://wccftech.com/how-to-hard-reset-lg-g5/\r\n\r\n\r\n---------------- ZTE Axon 7 ---------------------\r\n\r\nhttps://www.notebookcheck.com/Qualcomm-Snapdragon-400-MSM8928-vs-Qualcomm-Snapdragon-616-MSM8939v2_6034_7846.247552.0.html\r\n\r\n- 360 €\r\n- kein wechselbarer akku, schlecht wechselbar -> https://www.mobilescout.com/android/news/n77367/ZTE-Axon-7-teardown-video.html\r\n- 5,5\" display\r\n- lineage os\r\n\r\n-> axon 7 mini kostet nur ~240, aber nicht sicher ob es lineage os hat\r\n\r\n------------ Samsung S5 -------------------------\r\n\r\n- 260 €\r\n- wechselbarer akku\r\n- lineage\r\n- 5,1\"\r\n- etwas langsamer\r\nhttps://www.notebookcheck.com/Test-Samsung-Galaxy-S5-Smartphone.115431.0.html#toc--hnliche-ger-te\r\n\r\n\r\n\r\n-------------------\r\n-------------------\r\nsmartphone-CPU-benchmarks:\r\nhttps://www.notebookcheck.com/Qualcomm-Snapdragon-808-MSM8992-vs-Qualcomm-Snapdragon-820-MSM8996-vs-Qualcomm-Snapdragon-801-MSM8974AC-vs-Qualcomm-Snapdragon-400-MSM8928-vs-Qualcomm-Snapdragon-616-MSM8939v2_5975_7889_5680_6034_7846.247552.0.html\r\n','lineage, android, telefon','2017-09-17','',2),(368,'send emails with python','http://naelshiab.com/tutorial-send-email-python/\r\n\r\nhttp://kampis-elektroecke.de/?page_id=2324\r\n\r\noffizielle doku: https://docs.python.org/3/library/smtplib.html#smtplib.SMTP.send_message\r\n\r\n\r\n -------------- gpg/pgp ---------------\r\n\r\nhttps://access.redhat.com/solutions/1541303\r\n\r\ngpg in thunderbird: https://help.ubuntu.com/community/GnuPrivacyGuardHowto#Thunderbird\r\n\r\nhttps://stackoverflow.com/questions/10496902/pgp-signing-multipart-e-mails-with-python\r\n\r\n','mail, python','2017-09-15','',2),(369,'linux on android / chrome OS','according to this user, chrome os supports a ton of linux features, including the gentoo package manager: https://www.reddit.com/r/linux/comments/70l6p2/is_android_linux/dn5979p/\r\n\r\nHow to install a Linux desktop on your Android device: http://www.androidauthority.com/install-ubuntu-on-your-android-smartphone-765408/\r\n\r\nhttp://www.linux-magazine.com/Online/Features/Convert-an-Android-Device-to-Linux\r\n\r\n','smartphone, android, chromeos','2017-09-18','',2),(370,'git: show current project','This command shows what project git is working on in your current directory:\r\n\r\n~ $ git config --list | grep url\r\nremote.origin.url=https://github.com/rynnon/dotfiles-i3\r\n\r\n~/dev/system_automation $ git config --list | grep url\r\nremote.origin.url=https://kallithea.hornetsecurity.com/system_automation\r\n\r\n\r\n\"git branch\" works as well, but it\'s only useful if the branch names describe your project - \"hidpi\" is clear, but \"master\" is not.\r\n\r\n','git','2017-09-18','',2),(371,'git cheat sheet','common git commands on two pages in a pdf-document: https://education.github.com/git-cheat-sheet-education.pdf\r\n\r\nletzes \"git commit\" rückgängig machen (ohne Änderungen an den Datein rückgängig zu machen):\r\n\r\n   git reset --soft HEAD~1\r\n\r\ncommits auflisten:\r\n\r\n   git log --pretty=format:\'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset\' --abbrev-commit\r\n\r\ndelete local branch:\r\n\r\n   git branch -d feature/login\r\n\r\ndelete remote branch:\r\n\r\n   git push origin --delete feature/login\r\n\r\n->https://www.git-tower.com/learn/git/faq/delete-remote-branch\r\n\r\nwhen you want to force your code to be equal to the remote: \r\n\r\n   git reset --hard origin/master\r\n\r\ntruncate commit history when cloning (to 1 in this case):\r\n\r\n   git clone --depth 1 https://github.com/ChocolateBread799/sidebar','git','2021-06-13','',2),(372,'git cheat sheet link','common git commands on two pages in a pdf-document: https://education.github.com/git-cheat-sheet-education.pdf','git','2017-09-18','',2),(373,'tool for load testing a website','Synopsis: \"Locust is an easy-to-use, distributed, user load testing tool. It is intended for load-testing web sites (or other systems) and figuring out how many concurrent users a system can handle.\"\r\n\r\nAnd with python!\r\n\r\nhttp://docs.locust.io/en/latest/index.html','load, webentwicklung, locust','2017-09-18','',2),(374,'how to shorten virtualenv\'s source command','file: ~/bin/env:\r\n#!/bin/bash\r\nsource /home/$USER/.virtualenvs/$PROJECT/bin/activate\r\n\r\nin terminal:\r\n   source env\r\n\r\nor, since \'source\' is an alias for \'.\':\r\n   . env','cli, virtualenv','2017-09-19','',2),(376,'run firefox in tmpfs','https://www.verot.net/firefox_tmpfs.htm\r\n\r\nhttps://www.heise.de/forum/heise-online/News-Kommentare/Gefaehrdeter-Datenschutz-Firefox-loescht-lokale-Datenbanken-nicht/In-das-tmpfs-verbannt/posting-31069484/show/\r\n\r\n','firefox, tmpfs','2017-09-20','',2),(377,'SSL/HTTPS in Django','erklärung und gratis-Zertifikat von Mozilla: https://blog.mozilla.org/internetcitizen/2017/04/21/https-protect/\r\n\r\nweitere erklärung, was das macht: https://blog.instantssl.com/ssl/decoding-ssl-how-does-ssl-certificate-protect-against-hacking/\r\n\r\nso besorgt man sich ein Zertifikat und stellt es nginx zur Verfügung: https://simpleisbetterthancomplex.com/tutorial/2016/05/11/how-to-setup-ssl-certificate-on-nginx-for-django-application.html\r\n\r\nÜberblick über Djangos Sicherheits-Funktionen: https://docs.djangoproject.com/en/1.11/topics/security/\r\n\r\n','ssl, https, django, nginx','2017-09-20','',2),(379,'install geckodriver','download:\r\nhttps://github.com/mozilla/geckodriver/releases\r\n\r\nuncompress: \r\n   tar -xf geckodriver-v0.19.0-linux64.tar.gz\r\n\r\nmake executable:\r\n   cp geckodriver ~/bin/geckodriver\r\n   chmod +x ~/bin/geckodriver\r\n\r\n\r\n','geckodriver, testing, tdd, tar','2017-09-26','',2),(380,'install pip-packages as user','pip3 install --user virtualenvwrapper\r\n\r\n','pip, python','2017-09-26','',2),(381,'set key repeat delay and rate in commandline','http://linuxforcynics.com/how-to/set-keyboard-repeat-delay-and-rate\r\n\r\n   xset r rate 200 20\r\n\r\n\r\ni3wm + cinnamon:\r\nexport/import cinnamon settings:\r\nhttps://github.com/linuxmint/Cinnamon/wiki/Backing-up-and-restoring-your-cinnamon-settings-(dconf)\r\n\r\n   sudo apt install dconf-cli\r\n\r\n   dconf dump /org/cinnamon/ > backup_of_my_cinnamon_settings\r\n\r\n   dconf load /org/cinnamon/ < backup_of_my_cinnamon_settings\r\n\r\noverride cinnamon settings:\r\nhttps://wiki.archlinux.org/index.php/Cinnamon#Prevent_Cinnamon_from_overriding_xrandr.2Fxinput_configuration\r\n --> /etc/xdg/autostart/cinnamon-settings-daemon-keyboard.desktop\r\n\r\n\r\n','keyboard, cli, xset, cinnamon','2017-12-23','',2),(382,'cleaning up systemd startup process','https://www.linux.com/learn/cleaning-your-linux-startup-process\r\n\r\nThe average general-purpose Linux distribution launches all kinds of stuff at startup, including a lot of services that don\'t need to be running. Bluetooth, Avahi, ModemManager, ppp-dns… What are these things, and who needs them? \r\n\r\nSeriously, Ubuntu/Mint takes ages to boot, compared to Debian. Check:\r\n\r\n   systemd-analyze\r\n   systemd-analyze blame\r\n\r\n','systemd, analyze','2017-10-05','',2),(383,'how to set up a VM with GPU-passthrough','https://davidyat.es/2016/09/08/gpu-passthrough/\r\n\r\nthis is one of the linked ressources in the first article: https://forums.linuxmint.com/viewtopic.php?f=231&t=212692\r\n\r\n','virtualization, kvm, qemu','2017-10-08','',2),(384,'Eliminate screen tearing with AMD GPU on Ubuntu/Debian\r\n','https://cubethethird.wordpress.com/2016/06/14/eliminate-screen-tearing-with-amd-gpu-on-ubuntu/\r\n\r\nIn /usr/share/X11/xorg.conf.d/ directory, edit (or create) a file called 20-radeon.conf with the following contents:\r\n\r\n    Section \"Device\"\r\n        Identifier \"Radeon\"\r\n        Driver \"radeon\"\r\n        Option \"TearFree\" \"on\"\r\n    EndSection\r\n\r\n(appending it to 10-radeon.conf works,too)\r\n\r\n\r\n\r\n','amdgpu, vsync','2017-10-09','',2),(385,'AMD A8-7600 hardware-accelerated youtube-videos','Video mit mpv und hardware-beschleunigung starten:\r\n    \r\n    mpv --hwdec=vdpau --vo=vdpau https://youtu.be/cNr4VqabsNU?t=1418\r\n\r\n\r\nEinrichtung(Ubuntu 16.04):\r\n   \r\n   sudo apt install mesa-vdpau-drivers vdpauinfo\r\n   VDPAU_DRIVER=radeonsi\r\n    \r\nDamit wird die CPU-Nutzung von 70% (standard/software-decoding) auf 9% gesenkt, während die GPU-Nutzung (durch sudo radeontop gemessen) von 2% auf 8% ansteigt.\r\n\r\nhttps://wiki.archlinux.org/index.php/Hardware_video_acceleration#Verification\r\n\r\nhttps://wiki.gentoo.org/wiki/Mpv\r\n\r\nhttps://packages.ubuntu.com/xenial/mesa-vdpau-drivers\r\n\r\n\r\n','amd, mpv, youtube','2017-10-19','',2),(386,'trying to resolve issue with wifi dropping','https://www.reddit.com/r/linuxmint/comments/3lyd9o/recent_installed_mint_wifi_issue/\r\n\r\n-> https://askubuntu.com/questions/593734/ubuntu-14-04-wireless-is-fluctuating\r\n\r\n---\r\nanother try: \r\n\r\nautomatically reconnect via NetworkManager dispatcher script\r\nhttps://askubuntu.com/a/938336\r\n\r\n','network, network-manager, wifi','2017-11-06','',2),(387,'convert wav to mp3 from commandline','   \r\n   lame -V 0 \"track title.wav\"\r\n\r\nautomatically creates \"track title.mp3\" with variable bitrate at ~250 kbs. after typing \r\n\r\n   lame -V 0 \"t\r\n\r\nyou can press tab to autocomplete the title.\r\n\r\n\r\n','lame, audio, cli','2017-11-06','',2),(388,'fix for invisible underscores in terminal','add the following line to .Xresources:\r\n\r\nURxvt.lineSpace: 1\r\n\r\n','urxvt, font','2017-11-08','',2),(390,'change display resolution with xrandr (for VNC servers)','to decrease network load for remote desktop, set a smaller display:\r\n\r\n   xrandr --fb 800x800','xrandr, vnc','2017-11-10','',2),(392,'enable boost-frq in kaveri','per default, AMD A8-7600 seems to only scale up to 3.1 GHz on linux - it\'s capable of 3.8 GHz, though.\r\n\r\nAdd this option to /etc/default/grub:\r\n\r\nGRUB_CMDLINE_LINUX_DEFAULT=\"radeon.bapm=1\"\r\n\r\nhttps://www.reddit.com/r/linuxmint/comments/7dxje7/my_kaveri_apu_a87600_has_a_31ghz_base_clock_and/\r\n\r\ncheck with this:\r\n\r\n   sudo apt install linux-tools-4.10.0-37-generic\r\n   cpupower frequency-info\r\n   sudo watch -n 0,5 cpupower monitor\r\n\r\n','kaveri, amd, cpu','2017-11-19','',2),(394,'firefox: hide native tab bar, for tree style tabs','https://www.reddit.com/r/firefox/comments/736cji/how_to_hide_native_tabs_in_firefox_57_tree_style/\r\n\r\nadd the following to ~/.mozilla/firefox/xxxxxxxx.default/chrome/userChrome.css:\r\n\r\n@namespace url(\"http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul\");\r\n\r\n/* to hide the native tabs */\r\n#TabsToolbar {\r\n    visibility: collapse;\r\n}\r\n\r\n/* to hide the sidebar header */\r\n#sidebar-header {\r\n    visibility: collapse;\r\n}\r\n\r\n---\r\n2019/12/04 Update - userChrome.css Styles No Longer Enabled by Default\r\n\r\nApparently as of Firefox 69.0, the userChrome.css is no longer applied by default (thanks Mozilla). According to this ghacks post one must enable the following setting for the userChrome.css styles to take effect:\r\n\r\n- Load about:config in the Firefox address bar\r\n- Confirm that you will be careful\r\n- Search for toolkit.legacyUserProfileCustomizations.stylesheets\r\n- Set the value to True','firefox, tree-style-tabs','2021-05-08','',2),(395,'change keyboard layout from commandline','https://wiki.debian.org/Keyboard\r\n\r\nhttps://benohead.com/debian-change-the-keyboard-layout-from-the-console/\r\n\r\nthis command worked (might have to generate first):\r\n\r\nsetxkbmap de\r\n\r\n\r\n','keyboard, locale, layout','2017-12-23','',2),(397,'killswitch for internet for VPN drops','how to make a killswitch for your internet connection in case your VPN connection drops\r\n\r\nhttps://www.whonix.org/wiki/VPN-Firewall#How_to_use_VPN-Firewall\r\n\r\n','vpn, firewall','2017-12-12','',2),(398,'VPN setup','how to setup VPN:\r\n\r\n-> https://nordvpn.com/tutorials/linux/openvpn/\r\n\r\nNETWORK-MANAGER\r\n\r\nThis might work from the commandline as well (nmcli/nmtui) on Ubuntu 16.04, but not Debian 8. Debian 9 not tested, yet.\r\n\r\ninstall network-manager-vpn-gnome(openvpn + /etc/network/interfaces works, too, but saving username and password is too complicated)\r\n\r\nnm-connection-editor -> import config\r\n\r\nuse nmtui to activate (installed with network manager)\r\n\r\ncheck by visiting https://nordvpn.com, or with this command (returns nothing if VPN connection is not active):\r\n\r\n   ifconfig tun0 &> /dev/null && echo -e \"\\033[1;32m\" \"-- VPN is active --\" \"\\033[0m\"\r\n\r\nand for good measure, check for DNS leaks:\r\n\r\n   https://dnsleaktest.com\r\n\r\n\r\n\r\n','vpn, nmtui','2017-12-15','',2),(399,'tmux cheatsheet','start named session (socket): \r\n\r\n   tmux -L [name]\r\n\r\ndetach session:\r\n\r\n   Ctrl+b d\r\n\r\nreattach named session (socket):\r\n\r\n   tmux -L [name] a\r\n\r\ncreate new named session:\r\n\r\n   tmux new -s [name]\r\n\r\nattach named session:\r\n\r\n   tmux a -t [name]\r\n\r\nsplit pane horizontally (top and bottom):\r\n\r\n   Ctrl-b \"\r\n\r\nsplit pane vertically (left and right):\r\n\r\n   Ctrl-b %\r\n\r\nswitch pane:\r\n\r\n   Ctrl-b [arrow-key]\r\n\r\n\r\nmore info:\r\nhttps://linux.die.net/man/1/tmux\r\nhttps://leanpub.com/the-tao-of-tmux/read\r\n\r\n\r\n\r\n','tmux, cli','2017-12-15','',2),(400,'bash: move files that contain substring to directory','Move all files that contain a substring in their name to a certain directory:\r\n\r\n    mv *S01E1*.mp4 Gronkh-LITW\\[100-199\\]/\r\n\r\n\"Move all files with the substring \'S01E1\' that end with \'.mp4\' into the directory \'Gronkh-LITW[100-199]\'.\"\r\n\r\n','bash, mv, cli','2017-12-16','',2),(403,'configuring gnome-terminal','gnome-terminal is the default terminal in Ubuntu and Linux Mint.\r\n\r\n\r\nhide menubar:\r\n\r\nstart with gnome-terminal --hide-menubar\r\n\r\n\r\nshow menubar:\r\n\r\nrightclick -> tick show menubar\r\n\r\n\r\nincrease padding:\r\n\r\ncreate ~/.config/gtk-3.0/gtk.css\r\nadd these lines:\r\n\r\nVteTerminal {\r\n   padding: 10px;\r\n}\r\n\r\n','gnome-terminal','2017-12-23','',2),(404,'getting started with AwesomeWM','couple of links to get started with AwesomeWM:\r\n\r\nthread on reddit\r\nhttps://www.reddit.com/r/awesomewm/comments/4d13pu/awesomewm_for_beginners/\r\n\r\na user\'s guide, written for fellow users new to tiling wms:\r\nhttps://scaron.info/blog/getting-started-with-awesome.html\r\n\r\nMy first Awesome\r\nhttps://awesomewm.org/apidoc/documentation/07-my-first-awesome.md.html\r\n\r\nscreenshot thread on awesome\'s github:\r\nhttps://github.com/awesomeWM/awesome/issues/1395\r\n\r\n','awesome, wm, tiling','2017-12-26','',2),(405,'swap on Ubuntu','I used this guide to create a swap file:\r\nhttps://help.ubuntu.com/community/SwapFaq\r\n\r\nTo check the swappiness value:\r\n\r\n   cat /proc/sys/vm/swappiness\r\n\r\n\r\ncreate swapfile:\r\n \r\n   sudo fallocate -l 1g /mnt/1GiB.swap\r\n   sudo chmod 600 /mnt/1GiB.swap\r\n   sudo mkswap /mnt/1GiB.swap\r\n   sudo swapon /mnt/1GiB.swap\r\n   echo \'/mnt/1GiB.swap swap swap defaults 0 0\' | sudo tee -a /etc/fstab\r\n\r\n','swap, ubuntu','2017-12-28','',2),(406,'Full disk encryption ohne swap auf Debian 9','Reddit post:\r\nhttps://www.reddit.com/r/debian/comments/7n40zt/trying_to_set_up_encrypted_volume_manually/\r\n\r\n--> Antwort von /u/mfigueiredo\r\n\r\nIf you want to use crypto+LVM you have a guided installation recipe which asks you for that. If you want customization you create the crypto volume (as you did) and inside you create a lvm partition instead of ext4.\r\n\r\nOn the menu (above in your screenshot) \"Confire the logical volume manager\" you can create logical volumes (LV) to host partitions (such as ext4).\r\n\r\nIf you want to see screenshots from installation, take a look on: https://xo.tc/setting-up-full-disk-encryption-on-debian-9-stretch.html\r\n\r\n','luks, lvm, debian, swap','2017-12-31','',2),(407,'how to vectorize a raster image','http://goinkscape.com/how-to-vectorize-in-inkscape/','image, svg, inkscape, vektor','2018-01-05','',2),(410,'making xterm, ssh, tmux, htop, ranger and micro play nice','Why do terminal emulators suck so much?\r\n\r\nAnyway, here\'s how to make keybinds work in an ssh session with xterm (urxvt just doesn\'t work for this), tmux, and several ncurses apps (ranger, htop, micro):\r\n\r\nlocal host:\r\n# ~/.Xresources\r\nXTerm*faceName:Ubuntu Mono:Regular:size=11\r\nXTerm*internalBorder: 8\r\nXTerm*borderWidth: 0\r\nXTERM*termName: xterm\r\nXTerm*metaSendsEscape: true\r\nXTerm*selectToClipboard: true\r\nXTerm*VT100*translations: #override \\n\\\r\n    Ctrl Shift <Key>V:  insert-selection(CLIPBOARD) \\n\\\r\n    Ctrl Shift <Key>C:        select-end(CLIPBOARD) \\n\\\r\n\r\nremote host:\r\n# ~/.tmux.conf\r\nset -g default-terminal xterm\r\nset-window-option -g xterm-keys on\r\n\r\nOne caveat: \r\nhtop starts looking a little odd (colored row background end earlier).\r\n\r\n\r\n','xterm, ssh, tmux, ncurses','2018-01-06','',2),(413,'convert mp3 to wav','Ardour does not like lossy audio files. Here is how you do it:\r\n\r\n  sudo apt install mpg321\r\n  mpg321 -w output.wav input.mp3','audio, cli','2019-12-22','',2),(414,'convert mp4 to m4a (extract audio from video)','ffmpeg -i video.mp4 -vn -acodec copy audio.m4a','convert, audio','2019-12-30','',2),(416,'LXC autostart','https://www.cyberciti.biz/faq/how-to-auto-start-lxd-containers-at-boot-time-in-linux/\r\n\r\nFunktioniert auch  unter Debian 9 und lxd 4.12. Habe gerade mit folgendem Befehl das automatische Hochfahren vom Container cyan (Technikzeugs mit PHP+Apache)abgestellt, glaube ich zumindest:\r\n\r\n$ lxc config set cyan boot.autostart false','lxd, lxc, technikzeugs','2021-03-12','',2),(419,'uwsgi systemwide install','Running site with systemwide uwsgi-install:\r\n\r\n# uwsgi --ini /home/ubuntu/site-dev/technikzeug_uwsgi.ini --uid=ubuntu --gid=ubuntu\r\n\r\nStartup on system boot: Add above line to /etc/rc.local\r\n-> https://uwsgi-docs.readthedocs.io/en/latest/tutorials/Django_and_nginx.html#make-uwsgi-startup-when-the-system-boots','django, python, uwsgi, rc.local','2021-03-15','',2),(420,'find RAM info','https://www.cyberciti.biz/faq/check-ram-speed-linux/\r\n\r\nsudo dmidecode --type 17','ram, hardware','2021-03-20','',2),(421,'git: purge file from history','Musste altes README.md aus technikzeug-git löschen, weil lauter Passwörter drinstehen und ich das Projekt öffentlich machen will.\r\n\r\nReferenzen:\r\nhttps://www.axllent.org/docs/purge-files-from-git/\r\nhttps://refactorsaurusrex.com/posts/purge-a-file-or-directory-from-a-git-repository/\r\n\r\nBefehle:\r\ngit branch # sicherstellen, dass ich im richtigen branch bin\r\ngit commit -am \"blabla\"\r\ngit push # änderungen hochladen\r\nlxc snapshot django-cyan-dev before_git_purge # BACKUP\r\ngit filter-branch --index-filter \'git rm -rf --cached --ignore-unmatch README.md --prune-empty --tag-name-filter cat -- --all\r\ngit for-each-ref --format=\"%(refname)\" refs/original/ | xargs -n 1 git update-ref -d\r\nrm -Rf .git/logs .git/refs/original\r\ngit gc --prune=all --aggressive\r\ngit push origin javascript --force\r\ngit checkout master # muss wohl auf allen branches einzeln gemacht werden ...\r\ngit filter-branch --index-filter \'git rm -rf --cached --ignore-unmatch README.md --prune-empty --tag-name-filter cat -- --all\r\ngit for-each-ref --format=\"%(refname)\" refs/original/ | xargs -n 1 git update-ref -d\r\nrm -Rf .git/logs .git/refs/original\r\ngit gc --prune=all --aggressive\r\ngit checkout javascript\r\ntouch README.md # wir brauchen ja ein neues öffentliches README\r\ngit add README.md\r\ngit commit -am \"purged old README because it contained sensitive information, and made a new one\"\r\n\r\n# on production server (different container):\r\ngit fetch --all\r\ngit reset --hard origin/master\r\n# following steps might not be strictly necessary for the file purging operation, but I did it anyway\r\ngit pull origin javascript\r\ngit checkout javascript','git','2021-04-01','',2),(422,'set up and use javascript linter','# setup\r\ncd /path/to/projectfolder\r\nnpm init\r\nnpm install eslint --save-dev\r\necho \"node_modules\" >> .gitignore # den kram braucht keiner im git\r\nnpx eslint --init\r\n\r\n# use\r\nnpx eslint js/script.js','javascript, linter, eslint','2021-04-05','',2),(423,'rename git branch','git branch -m old-name new-name\r\n\r\nTo delete the old branch on remote (suppose, the name of remote is origin, which is by default), use the following command:\r\n\r\n   git push origin --delete old-name\r\n\r\nThen you should push the new branch to remote:\r\n\r\n   git push origin new-name\r\n\r\nTo reset the upstream branch for the new-name local branch use the -u flag with the git push command:\r\n\r\n   git push origin -u new-name\r\n\r\nIf branch is set as main branch in remote repository, might need to set new branch as default before deleting the old one.','git, branch','2021-04-06','',2),(424,'find out ethernet speed','Type the following command to get speed for eth0:\r\n      $ ethtool eth0 | less\r\nOR\r\n      $ ethtool eth0 | grep -i speed\r\n\r\nYou can also use the following two commands:\r\n     $ dmesg | grep eth0 | grep up\r\n(my output:)\r\n     $ dmesg | grep eth0 \r\n     [26824.837937] e1000e: enp0s31f6 NIC Link is Up 1000 Mbps Full Duplex, Flow Control: Rx/Tx\r\n\r\n-> https://www.cyberciti.biz/faq/howto-determine-ethernet-connection-speed/','ethernet, nic','2021-04-12','',2),(425,'setup listking','This article documents how I installed the List King app in a fresh Ubuntu container.\r\n\r\n# create container\r\nlxc launch ubuntu:20.04 listking\r\n# enter container\r\nlxc exec listking -- /bin/bash\r\n\r\napt update \r\napt upgrade\r\napt install ranger tmux nginx \r\napt install --no-install-recommends npm \r\n# someone compiled ubuntu\'s repo micro with the debug flag, which leaves \'log.txt\' files everywhere, so we use release from github instead\r\nwget https://github.com/zyedidia/micro/releases/download/v2.0.8/micro-2.0.8-amd64.deb\r\ndpkg -i micro-2.0.8-amd64.deb\r\nrm micro-2.0.8-amd64.deb\r\nnpm install -g n\r\nn 12 # node.js version 12.22.11\r\n\r\n### project-specific setup\r\nsu ubuntu\r\ncd\r\nssh-keygen \r\neval `ssh-agent`             # start ssh-agent\r\nssh-add ~/.ssh/id_rsa\r\ncat ~/.ssh/id_rsa.pub\r\n# add key to bitbucket account, see https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/#-Step-3.-Add-the-public-key-to-your-Account-settings\r\n# test whether you can access bitbucket with your local account\r\nssh -T git@bitbucket.org \r\n\r\n### backend\r\ngit clone -b local git@bitbucket.org:marneu/listking-backend.git\r\ncd listking-backend\r\nnpm install \r\nnpm start\r\n\r\n# test backend from outside of the container\r\nhttp 10.98.228.87:3000/ping\r\nfirefox http://10.98.228.87:3000/explorer/\r\n\r\n# test nginx from outside of the container\r\nfirefox http://10.98.228.87\r\n# should open \"Welcome to nginx!\r\n\r\n### listking-frontend\r\nsudo su\r\ncd /var/www\r\nchown -R ubuntu:www-data listking\r\ncd listking\r\nsu ubuntu\r\ncat ~/.ssh/id_rsa.pub\r\n# add key to listking-repository as well\r\ngit clone -b plain-javascript git@bitbucket.org:marneu/listking.git .\r\nsudo su\r\ncp nginx-site-default /etc/nginx/sites-available/default\r\nmicro /etc/nginx/sites-available/default\r\n# change \'root /var/www/workspace;\'  to \'root /var/www;\'\r\n\r\n# test frontend\r\nfirefox http://10.98.228.87/listking/\r\nfirefox http://10.98.228.87/listking/login.html\r\n\r\n# now we get CORS-related errors when trying to access the API from the app frontend. let\'s remedy:\r\n# change baseUrl in /var/www/js/*.js\r\nconst baseUrl = \'http://10.98.228.87:3000\';\r\nsudo service nginx restart\r\n\r\n# autostart backend\r\nsudo su\r\ntouch /etc/rc.local | sudo micro /etc/rc.local\r\n# add the following command\r\nnpm run --prefix /home/ubuntu/listking-backend start\r\ntouch /etc/systemd/system/rc-local.service\r\n# fill with unit file from here: https://www.bitblokes.de/etc-rc-local-unter-ubuntu-20-04-linux-mint-20-wieder-aktivieren/\r\nmicro /etc/systemd/system/rc-local.service \r\n\r\n[Unit]\r\n Description=/etc/rc.local Compatibility\r\n ConditionPathExists=/etc/rc.local\r\n\r\n[Service]\r\n Type=forking\r\n ExecStart=/etc/rc.local start\r\n TimeoutSec=0\r\n StandardOutput=tty\r\n RemainAfterExit=yes\r\n SysVStartPriority=99\r\n\r\n[Install]\r\n WantedBy=multi-user.target\r\n\r\nchmod +x /etc/rc.local\r\nsystemctl enable rc-local\r\nexit\r\nlxc restart listking\r\n# check whether node is running in htop\r\n\r\n# make snapshot after setup\r\nlxc snapshot listking after-setup\r\n# check whether snapshot was really created\r\nlxc info listking','lxc, lxd, listking, javascript, loopback, nodejs','2021-04-23','',2),(426,'snap cheatsheet','# update everything\r\nsudo snap refresh\r\n\r\n# list everything, including old versions:\r\nsnap list --all\r\n\r\n# install:\r\nsudo snap install micro --classic\r\n\r\n# uninstall:\r\nsudo snap remove micro\r\n\r\n# delete a revision:\r\nsudo snap remove micro --revision=2995\r\n\r\n# disable a package:\r\nsudo snap disable micro\r\n\r\n# disable snappy:\r\nsudo systemctl disable --now snapd\r\nsudo systemctl disable --now snapd.socket\r\nsudo systemctl disable --now snapd.seeded','snap, snappy, ubuntu','2021-04-29','',2),(427,'LXD cheatsheet','# launch (start a new instance, might download image from internet first) a debian container\r\nlxc launch images:debian/11 container-name\r\n\r\n# enter a container\r\nlxc exec container-name -- bash\r\n\r\n# edit global image expiry in, measured in days (default == 10)\r\nsudo lxc config set images.remote_cache_expiry 60\r\n\r\n# check size and available space of storage pool(s)\r\nlxc storage info default\r\n\r\n# check size of everything lxd\r\nsudo du -sh /var/snap/lxd\r\n\r\n# check which images are stored locally\r\nlxc image list\r\n\r\n# view online images (11 == Debian Bullseye, amd64 == cpu architecture x86_64\r\nlxc image list images:debian/11/amd64\r\n\r\n# view online images of one arch, but different release versions\r\nlxc image list images:debian | grep amd64\r\n\r\n# export a container\r\nlxc stop container-name\r\nlxc export container-name [container-backup]\r\ncp container-backup ~/TERA/os-images/container-temp/\r\n# export with compression\r\nlxc export --compression gzip container [backup.tar.gz]\r\n# log into other machine\r\nlxc import container-name-backup new-name\r\nlxc start new-name\r\n\r\n### save a base image \r\nlxc publish alpine-1 --alias alpine-dev-base\r\n# check that it worked \r\nlxc image list\r\n# alternatively, clone it\r\nlxc copy alpine-1 alpine-1-clone\r\n# check that cloing worked\r\nlxc list\r\n\r\n### profiles\r\n# reference: https://linuxcontainers.org/lxd/docs/master/instances\r\nlxc profile list                           # show available profiles\r\nlxc profile show default                   # show settings of profile \"default\"\r\n# limit availability of CPU cores per instance\r\nlxc profile set default limits.cpu=3       \r\n# limit available RAM; \"2GB\" is also a valid value\r\nlxc profile set default limits.memory=2GiB\r\n\r\n### snapshots\r\n\r\n# create snapshot\r\nlxc snapshot django-cyan descriptive-snapshot-title\r\n\r\n# revert container to previous snapshot\r\nlxc restore listking-install-test before_install\r\n\r\n# delete snapshot\r\nlxc delete django-cyan-3/snap1','lxc, lxd, containers','2021-08-18','',2),(428,'cut videos with ffmpeg','ffmpeg -i input.mp4 -vcodec copy -acodec copy -ss 01:17:00 -to 05:05:00 output1.mp4\r\n\r\n- Parameter \"-t\": duration\r\n- Parameter \"-to\": endpoint\r\n- specific codec doesn\'t matter for this command\r\n- it\'s I/O bound, so don\'t run this command on a local machine when the file is on a remote machine','ffmpeg, video, mp4','2021-05-15','',2),(429,'HDD / SSD stuff','display manufacturer information: \r\n\r\n     sudo hdparm -I /dev/sda\r\n\r\ntrim ssd manually:\r\n\r\n     sudo fstrim --verbose /\r\n\r\nenable automatic weekly fstrim:\r\n\r\n    sudo systemctl enable fstrim.timer','ssd, hdd, drives,','2021-05-25','',2),(430,'xinput/libinput mouse settings','#!/bin/sh\r\nSENSITIVITY=0.4\r\nACCELERATION=0.9\r\n\r\nxinput set-prop 9  \'libinput Accel Speed\' $ACCELERATION\r\nxinput set-prop 9 \"Coordinate Transformation Matrix\" $SENSITIVITY 0 0 0 $SENSITIVITY 0 0 0 1\r\n# print settings\r\nxinput list-props 9\r\n\r\n-> https://wiki.archlinux.org/title/Mouse_acceleration','mouse, xinput, libinput','2021-06-11','',2),(431,'luks/dm-crypt cheatsheet','Guide for Debian 11 install (most are for Debian 10): \r\n- https://techencyclopedia.wordpress.com/2020/04/21/debian-10-manual-partition-for-boot-swap-root-home-tmp-srv-var-var-mail-var-log/\r\n- https://unix.stackexchange.com/questions/577379/how-can-i-install-debian-with-full-disk-encryption-and-a-custom-sized-swapfile\r\n\r\n#  mount nvme\r\nsudo cryptsetup luksOpen /dev/nvme0n1p5 NVME\r\nsudo mount /dev/mapper/LVM_VG-root NV-MOUNT\r\n\r\n#  unmount nvme\r\nfuser -kim /dev/mapper/NVME\r\nsudo umount -l /dev/mapper/LVM_VG-root\r\nsudo cryptsetup luksClose LVM_VG-root\r\nsudo cryptsetup luksClose LVM_VG-swap\r\nsudo cryptsetup luksClose NV-MOUNT','luks, dm-crypt, mounting','2021-07-15','',2),(432,'ondemand cpu frequency governor','prüfen: \r\n   cpupower -c 0 frequency-info\r\n\r\nWenn \"ondemand\" nicht verfügbar und driver \"intel_pstate\":\r\n- sudo apt install acpi\r\n- /etc/default/grub -> GRUB_CMDLINE_LINUX_DEFAULT=\"quiet intel_pstate=disable acpi=force\"\r\n- im BIOS SpeedStep aktivieren\r\n- sudo cpufreq-set --related --governor ondemand\r\n\r\nFrequenz beobachten: \r\n   watch \'lscpu | grep \"CPU MHz\"\'','cpu, acpi, pstate, intel, ondemand','2021-08-11','',2),(433,'docker gotchas','Ubuntu 20.04:\r\n\r\n- by default, every docker command needs to be prefaced with \'sudo\'\r\n\r\n- lxc config set deploy-container security.nesting true ->https://stackoverflow.com/questions/46645910/docker-rootfs-linux-go-permission-denied-when-mounting-proc\r\n\r\n- creating a persistent database ->https://betterprogramming.pub/persistent-databases-using-dockers-volumes-and-mongodb-9ac284c25b39\r\n\r\n- run nginx container that works ->https://stackoverflow.com/a/62622763 \r\n   sudo docker run -P --net=host','lxd, docker, mongodb','2021-08-18','',2),(434,'Example-Artikel','This is a test.','test, example','2021-09-29','',3);
/*!40000 ALTER TABLE `artikel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add permission',2,'add_permission'),(5,'Can change permission',2,'change_permission'),(6,'Can delete permission',2,'delete_permission'),(7,'Can add user',3,'add_user'),(8,'Can change user',3,'change_user'),(9,'Can delete user',3,'delete_user'),(10,'Can add group',4,'add_group'),(11,'Can change group',4,'change_group'),(12,'Can delete group',4,'delete_group'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add artikel',7,'add_artikel'),(20,'Can change artikel',7,'change_artikel'),(21,'Can delete artikel',7,'delete_artikel'),(22,'Can view artikel',7,'view_artikel'),(23,'Can view log entry',1,'view_logentry'),(24,'Can view permission',2,'view_permission'),(25,'Can view group',4,'view_group'),(26,'Can view user',3,'view_user'),(27,'Can view content type',5,'view_contenttype'),(28,'Can view session',6,'view_session');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$260000$aHYlbRWiglUV4NanINoiDN$/6RkjkenVmThkvjkOQ7Xducpa32Ha2Ckk8dXzzhz8co=','2021-09-29 10:16:28.612753',1,'admin','','','marlon.neumann@mailbox.org',1,1,'2018-01-20 00:08:34.000000'),(2,'pbkdf2_sha256$260000$VmVludcKjolRe4TuMenUIw$K5Y7tA4DfvO/g3yR3jRuY6mTw1N0RHCQWzFr9CjYHIg=','2021-09-28 22:26:14.376114',0,'marneu','','','marlon.neumann@mailbox.org',1,1,'2021-03-12 14:02:44.000000'),(3,'pbkdf2_sha256$260000$wBJtEIAtyFamYoF6f2Mzv3$Vubk2LRnlfhiMlk8PHZ0t73kgEpCmXRDOhc5AAaa4P4=','2021-09-29 10:15:23.525123',0,'example-user','Example','User','user@example.com',1,1,'2021-09-29 10:12:59.000000');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
INSERT INTO `auth_user_user_permissions` VALUES (1,2,19),(2,2,20),(3,2,21),(4,3,19),(5,3,20),(6,3,21),(7,3,22);
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext DEFAULT NULL,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=379 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2018-01-20 00:16:52.197266','409','Rails-App preloading',3,'',7,1),(2,'2018-01-20 00:17:50.897884','411','yo',1,'[{\"added\": {}}]',7,1),(3,'2018-01-20 00:41:19.969993','412','ij',1,'[{\"added\": {}}]',7,1),(4,'2018-01-21 11:35:52.427156','245','django/python bücher',2,'[{\"changed\": {\"fields\": [\"titel\", \"text\"]}}]',7,1),(5,'2019-12-22 03:41:02.636650','413','convert mp3 to wav',1,'[{\"added\": {}}]',7,1),(6,'2019-12-22 03:41:21.628178','413','convert mp3 to wav',2,'[]',7,1),(7,'2019-12-22 03:41:59.987619','413','convert mp3 to wav',2,'[{\"changed\": {\"fields\": [\"text\"]}}]',7,1),(8,'2019-12-22 03:42:17.304570','413','convert mp3 to wav',2,'[]',7,1),(9,'2019-12-30 00:53:49.966001','414','convert mp4 to m4a (extract audio from video)',1,'[{\"added\": {}}]',7,1),(10,'2020-02-10 17:54:30.463095','97','save file locations in spielen',2,'[{\"changed\": {\"fields\": [\"text\"]}}]',7,1),(11,'2021-03-12 14:01:35.460905','2','marneu',1,'[{\"added\": {}}]',3,1),(12,'2021-03-12 14:03:09.481751','2','marneu',2,'[{\"changed\": {\"fields\": [\"email\", \"is_staff\", \"user_permissions\", \"last_login\", \"date_joined\"]}}]',3,1),(13,'2021-03-12 14:04:51.984513','415','test',1,'[{\"added\": {}}]',7,2),(14,'2021-03-12 14:05:07.239206','415','test',2,'[]',7,2),(15,'2021-03-12 14:05:24.719008','415','test',3,'',7,2),(16,'2021-03-12 14:07:30.249947','411','yo',3,'',7,2),(17,'2021-03-12 14:10:03.475570','412','ij',3,'',7,2),(18,'2021-03-12 14:19:51.884025','416','LXC autostart',1,'[{\"added\": {}}]',7,2),(19,'2021-03-12 17:09:41.727775','416','LXC autostart',2,'[{\"changed\": {\"fields\": [\"text\"]}}]',7,2),(20,'2021-03-12 17:09:58.208637','416','LXC autostart',2,'[{\"changed\": {\"fields\": [\"text\"]}}]',7,2),(21,'2021-03-13 13:12:16.210139','417','test',1,'[{\"added\": {}}]',7,2),(22,'2021-03-13 13:28:47.732698','418','cli-test',2,'[{\"changed\": {\"fields\": [\"datum\"]}}]',7,2),(23,'2021-03-15 16:40:52.768038','419','uwsgi-test',1,'[{\"added\": {}}]',7,2),(24,'2021-03-15 16:41:23.330332','419','uwsgi-test',2,'[{\"changed\": {\"fields\": [\"text\"]}}]',7,2),(25,'2021-03-15 17:07:11.464306','419','uwsgi running',2,'[{\"changed\": {\"fields\": [\"titel\", \"text\"]}}]',7,2),(26,'2021-03-15 17:07:46.484515','419','uwsgi systemwide install',2,'[{\"changed\": {\"fields\": [\"titel\", \"tags\"]}}]',7,2),(27,'2021-03-15 18:31:08.488080','417','test',3,'',7,2),(28,'2021-03-15 18:31:14.796598','418','cli-test',3,'',7,2),(29,'2021-03-20 23:03:44.150960','420','find RAM info',1,'[{\"added\": {}}]',7,2),(30,'2021-04-01 13:55:25.140579','421','git: purge file from history',1,'[{\"added\": {}}]',7,2),(31,'2021-04-02 07:11:16.166509','421','git: purge file from history',2,'[{\"changed\": {\"fields\": [\"text\"]}}]',7,2),(32,'2021-04-05 15:04:07.332859','422','set up and use javascript linter',1,'[{\"added\": {}}]',7,2),(33,'2021-04-05 16:48:22.323027','262','schöne Webseiten',2,'[{\"changed\": {\"fields\": [\"text\"]}}]',7,2),(34,'2021-04-06 18:01:40.443060','423','rename git branch',1,'[{\"added\": {}}]',7,2),(35,'2021-04-06 20:58:38.486409','423','rename git branch',2,'[{\"changed\": {\"fields\": [\"text\"]}}]',7,2),(36,'2021-04-12 14:30:34.115560','424','find out ethernet speed',1,'[{\"added\": {}}]',7,2),(37,'2021-04-23 03:27:31.305167','425','setup listking/javascript',1,'[{\"added\": {}}]',7,2),(38,'2021-04-23 03:53:22.273683','425','setup listking',2,'[{\"changed\": {\"fields\": [\"titel\", \"text\", \"tags\"]}}]',7,2),(39,'2021-04-23 04:15:18.531962','425','setup listking',2,'[{\"changed\": {\"fields\": [\"text\"]}}]',7,2),(40,'2021-04-23 05:05:22.463160','425','setup listking',2,'[{\"changed\": {\"fields\": [\"text\"]}}]',7,2),(41,'2021-04-23 05:21:16.617803','425','setup listking',2,'[{\"changed\": {\"fields\": [\"text\"]}}]',7,2),(42,'2021-04-23 05:33:20.129445','425','setup listking',2,'[{\"changed\": {\"fields\": [\"text\"]}}]',7,2),(43,'2021-04-23 05:34:43.758443','425','setup listking',2,'[{\"changed\": {\"fields\": [\"text\"]}}]',7,2),(44,'2021-04-25 09:13:55.113213','425','setup listking',2,'[{\"changed\": {\"fields\": [\"text\", \"tags\"]}}]',7,2),(45,'2021-04-29 19:06:08.474728','426','snap cheatsheet',1,'[{\"added\": {}}]',7,2),(46,'2021-04-29 19:06:31.955874','426','snap cheatsheet',2,'[{\"changed\": {\"fields\": [\"text\"]}}]',7,2),(47,'2021-04-30 13:26:33.951814','427','LXD cheatsheet',1,'[{\"added\": {}}]',7,2),(48,'2021-04-30 13:55:18.418305','427','LXD cheatsheet',2,'[{\"changed\": {\"fields\": [\"text\"]}}]',7,2),(49,'2021-04-30 14:04:00.227490','427','LXD cheatsheet',2,'[{\"changed\": {\"fields\": [\"text\"]}}]',7,2),(50,'2021-05-01 15:56:24.510490','427','LXD cheatsheet',2,'[{\"changed\": {\"fields\": [\"text\"]}}]',7,2),(51,'2021-05-01 16:01:01.453953','427','LXD cheatsheet',2,'[{\"changed\": {\"fields\": [\"text\"]}}]',7,2),(52,'2021-05-04 00:26:04.585716','427','LXD cheatsheet',2,'[{\"changed\": {\"fields\": [\"text\"]}}]',7,2),(53,'2021-05-04 00:27:12.204891','427','LXD cheatsheet',2,'[{\"changed\": {\"fields\": [\"text\"]}}]',7,2),(54,'2021-05-04 00:27:50.428840','427','LXD cheatsheet',2,'[{\"changed\": {\"fields\": [\"text\"]}}]',7,2),(55,'2021-05-08 22:04:14.657709','394','firefox: hide native tab bar, for tree style tabs',2,'[{\"changed\": {\"fields\": [\"text\", \"datum\"]}}]',7,2),(56,'2021-05-13 12:56:25.520759','394','firefox: hide native tab bar, for tree style tabs',2,'[]',7,2),(57,'2021-05-15 20:34:19.103707','428','cut videos with ffmpeg',1,'[{\"added\": {}}]',7,2),(58,'2021-05-15 20:34:34.208142','428','cut videos with ffmpeg',2,'[{\"changed\": {\"fields\": [\"text\"]}}]',7,2),(59,'2021-05-15 20:35:36.549630','428','cut videos with ffmpeg',2,'[{\"changed\": {\"fields\": [\"text\"]}}]',7,2),(60,'2021-05-21 04:09:10.979862','372','git cheat sheet link',2,'[{\"changed\": {\"fields\": [\"titel\", \"text\"]}}]',7,2),(61,'2021-05-21 05:14:49.036551','371','git cheat sheet',2,'[{\"changed\": {\"fields\": [\"text\"]}}]',7,2),(62,'2021-05-25 03:17:40.000372','429','HDD / SSD stuff',1,'[{\"added\": {}}]',7,2),(63,'2021-06-11 16:22:01.436561','430','xinput/libinput mouse settings',1,'[{\"added\": {}}]',7,2),(64,'2021-06-11 16:22:20.429147','430','xinput/libinput mouse settings',2,'[{\"changed\": {\"fields\": [\"text\"]}}]',7,2),(65,'2021-06-11 21:07:19.224539','428','cut videos with ffmpeg',2,'[{\"changed\": {\"fields\": [\"text\"]}}]',7,2),(66,'2021-06-13 21:16:36.929406','371','git cheat sheet',2,'[{\"changed\": {\"fields\": [\"text\", \"datum\"]}}]',7,2),(67,'2021-07-15 20:19:03.224717','431','luks/dm-crypt cheatsheet',1,'[{\"added\": {}}]',7,2),(68,'2021-07-15 20:23:30.678730','431','luks/dm-crypt cheatsheet',2,'[{\"changed\": {\"fields\": [\"text\"]}}]',7,2),(69,'2021-08-11 20:35:06.421158','432','ondemand cpu frequency governor',1,'[{\"added\": {}}]',7,2),(70,'2021-08-18 13:58:32.737716','427','LXD cheatsheet',2,'[{\"changed\": {\"fields\": [\"text\", \"datum\"]}}]',7,2),(71,'2021-08-18 14:01:17.367093','433','docker gotchas',1,'[{\"added\": {}}]',7,2),(72,'2021-08-18 14:31:10.959372','433','docker gotchas',2,'[{\"changed\": {\"fields\": [\"text\"]}}]',7,2),(73,'2021-08-19 13:53:16.916963','427','LXD cheatsheet',2,'[{\"changed\": {\"fields\": [\"text\"]}}]',7,2),(74,'2021-09-16 16:59:38.493403','427','LXD cheatsheet',2,'[{\"changed\": {\"fields\": [\"Text\"]}}]',7,2),(75,'2021-09-16 17:17:46.336584','427','LXD cheatsheet',2,'[]',7,2),(76,'2021-09-16 21:23:27.028445','433','docker gotchas',2,'[{\"changed\": {\"fields\": [\"Text\"]}}]',7,2),(77,'2021-09-16 21:23:39.967708','433','docker gotchas',2,'[{\"changed\": {\"fields\": [\"Text\"]}}]',7,2),(78,'2021-09-17 11:22:43.752336','427','LXD cheatsheet',2,'[{\"changed\": {\"fields\": [\"Text\"]}}]',7,2),(79,'2021-09-17 11:41:27.590945','427','LXD cheatsheet',2,'[{\"changed\": {\"fields\": [\"Text\"]}}]',7,2),(80,'2021-09-17 11:42:27.987936','427','LXD cheatsheet',2,'[{\"changed\": {\"fields\": [\"Text\"]}}]',7,2),(81,'2021-09-23 21:16:59.623977','433','docker gotchas',2,'[]',7,2),(82,'2021-09-26 23:44:09.660132','433','docker gotchas',2,'[]',7,2),(83,'2021-09-29 10:10:37.661983','1','admin',2,'[{\"changed\": {\"fields\": [\"Email address\"]}}]',3,1),(84,'2021-09-29 10:13:00.116333','3','example-user',1,'[{\"added\": {}}]',3,1),(85,'2021-09-29 10:14:49.282288','3','example-user',2,'[{\"changed\": {\"fields\": [\"First name\", \"Last name\", \"Email address\", \"Staff status\", \"User permissions\", \"Last login\"]}}]',3,1),(86,'2021-09-29 10:15:57.351110','434','Example-Artikel',1,'[{\"added\": {}}]',7,3),(87,'2021-09-29 10:19:05.015932','328','i3 over mint: install lightdm',3,'',7,1),(88,'2021-09-29 10:19:05.023899','327','grub configuration',3,'',7,1),(89,'2021-09-29 10:19:05.034779','326','Share wireless Internet connection through ethernet',3,'',7,1),(90,'2021-09-29 10:19:05.041383','325','bunsen -> stretch',3,'',7,1),(91,'2021-09-29 10:19:05.049793','324','wlan-repeater/accesspoint',3,'',7,1),(92,'2021-09-29 10:19:05.053834','323','search and remove PPAs',3,'',7,1),(93,'2021-09-29 10:19:05.061881','322','neue Grafikkarte für Minecraft',3,'',7,1),(94,'2021-09-29 10:19:05.068649','321','sensor-programmierung rpi/python',3,'',7,1),(95,'2021-09-29 10:19:05.093517','320','Fonts',3,'',7,1),(96,'2021-09-29 10:19:05.100379','319','raspbian auf sd-karte bringen, backup',3,'',7,1),(97,'2021-09-29 10:19:05.104825','318','Anwendung über ssh auf remote system (linux) starten',3,'',7,1),(98,'2021-09-29 10:19:05.112596','317','automatic login on raspbian jessie',3,'',7,1),(99,'2021-09-29 10:19:05.119694','316','ruby version managers',3,'',7,1),(100,'2021-09-29 10:19:05.127749','315','deploying django',3,'',7,1),(101,'2021-09-29 10:19:05.134896','314','migration: populate database from yaml-file in django',3,'',7,1),(102,'2021-09-29 10:19:05.139114','313','YAML in Python 3',3,'',7,1),(103,'2021-09-29 10:19:05.149869','312','network-config NetworkManager',3,'',7,1),(104,'2021-09-29 10:19:05.156965','311','How to create a systemd service',3,'',7,1),(105,'2021-09-29 10:19:05.161161','310','colorscheme switching scripts',3,'',7,1),(106,'2021-09-29 10:19:05.169936','309','ssmtp: ',3,'',7,1),(107,'2021-09-29 10:19:05.177902','308','logout/shutdown/usw-dialog: obshutdown',3,'',7,1),(108,'2021-09-29 10:19:05.185185','307','Mokkatorte',3,'',7,1),(109,'2021-09-29 10:19:05.193002','306','Schlagsahne',3,'',7,1),(110,'2021-09-29 10:19:05.199105','305','ricing: web startpage',3,'',7,1),(111,'2021-09-29 10:19:05.206137','304','i3 window manager',3,'',7,1),(112,'2021-09-29 10:19:05.209185','303','automated ricing',3,'',7,1),(113,'2021-09-29 10:19:05.215996','302','ricing: terminal-themes',3,'',7,1),(114,'2021-09-29 10:19:05.225790','301','Supercollider stuff',3,'',7,1),(115,'2021-09-29 10:19:05.230233','300','ricing: rxvt-terminal, ranger',3,'',7,1),(116,'2021-09-29 10:19:05.236277','299','mpd + ncmpcpp',3,'',7,1),(117,'2021-09-29 10:19:05.244489','298','debian, bash: run commands as script',3,'',7,1),(118,'2021-09-29 10:19:05.248615','297','autostart unter linux',3,'',7,1),(119,'2021-09-29 10:19:05.251922','296','mit nmap IP-adressen im netzwerk herausfinden',3,'',7,1),(120,'2021-09-29 10:19:05.259417','295','nfs einrichten, um netzwerk-dateisystem lokal zu mounten',3,'',7,1),(121,'2021-09-29 10:19:05.270323','294','ext4 festplatten-rettung',3,'',7,1),(122,'2021-09-29 10:19:05.277305','293','itch io games worth trying',3,'',7,1),(123,'2021-09-29 10:19:05.281520','292','multi-process firefox unter debian',3,'',7,1),(124,'2021-09-29 10:19:05.288760','291','test and repair drives',3,'',7,1),(125,'2021-09-29 10:19:05.299559','290','XMPP-Protokoll, aka jabber - ein Protokoll für Instant Messenging',3,'',7,1),(126,'2021-09-29 10:19:05.306476','289','git pull',3,'',7,1),(127,'2021-09-29 10:19:05.314030','288','windows 7 neu installieren für neuen PC',3,'',7,1),(128,'2021-09-29 10:19:05.320374','287','Mit rausgezoomten Webseiten auf dem Smartphone umgehen',3,'',7,1),(129,'2021-09-29 10:19:05.324496','286','web framework benchmark',3,'',7,1),(130,'2021-09-29 10:19:05.332989','285','Witcher 3 Optimization Guide',3,'',7,1),(131,'2021-09-29 10:19:05.343083','284','set default background-color in firefox',3,'',7,1),(132,'2021-09-29 10:19:05.351092','283','i5-build',3,'',7,1),(133,'2021-09-29 10:19:05.358472','282','using APIs with jQuery',3,'',7,1),(134,'2021-09-29 10:19:05.366223','281','to do freitag',3,'',7,1),(135,'2021-09-29 10:19:05.370548','280','Funksteckdosen: Herausfinden des Systemcodes',3,'',7,1),(136,'2021-09-29 10:19:05.377981','279','Django und AJAX',3,'',7,1),(137,'2021-09-29 10:19:05.385787','278','mounting vfat (debian)',3,'',7,1),(138,'2021-09-29 10:19:05.393958','277','rpi displayauflösung lösung',3,'',7,1),(139,'2021-09-29 10:19:05.401174','276','SD-Karten-Backup',3,'',7,1),(140,'2021-09-29 10:19:05.406106','275','bunsen manuell installieren (z.b. über raspbian)',3,'',7,1),(141,'2021-09-29 10:19:05.414804','274','neuer 2.1 lautsprecher',3,'',7,1),(142,'2021-09-29 10:19:05.419546','273','ubuntu tablet',3,'',7,1),(143,'2021-09-29 10:19:05.426822','272','OpenWRT/DD-WRT einrichten',3,'',7,1),(144,'2021-09-29 10:19:05.431083','271','RPi Zero Ethernet',3,'',7,1),(145,'2021-09-29 10:19:05.435935','270','vfat mounten mit bindfs',3,'',7,1),(146,'2021-09-29 10:19:05.443467','269','equalizing mpd',3,'',7,1),(147,'2021-09-29 10:19:05.450945','268','prozess einfrieren',3,'',7,1),(148,'2021-09-29 10:19:05.455786','267','parted',3,'',7,1),(149,'2021-09-29 10:19:05.462799','266','ricing',3,'',7,1),(150,'2021-09-29 10:19:05.469944','265','nützliche Befehle unter Debian',3,'',7,1),(151,'2021-09-29 10:19:05.477974','264','debian handbook',3,'',7,1),(152,'2021-09-29 10:19:05.484825','263','unattended_installation_of_a_debian_package',3,'',7,1),(153,'2021-09-29 10:19:05.486821','262','schöne Webseiten',3,'',7,1),(154,'2021-09-29 10:19:05.493774','261','externe festplatten unter Linux mit Schreibrechten mounten',3,'',7,1),(155,'2021-09-29 10:19:05.500852','260','Tageslichtwecker mit RPi',3,'',7,1),(156,'2021-09-29 10:19:05.505178','259','sprachsteuerung auf dem raspberry',3,'',7,1),(157,'2021-09-29 10:19:05.518643','258','ubuntu für rpi installieren',3,'',7,1),(158,'2021-09-29 10:19:05.525847','257','steam hangs on \"preparing to launch\" in mint',3,'',7,1),(159,'2021-09-29 10:19:05.531738','256','docker: software container, die abhängigkeiten enthalten',3,'',7,1),(160,'2021-09-29 10:19:05.538684','255','docker: software container, die abhängigkeiten enthalten',3,'',7,1),(161,'2021-09-29 10:19:05.545897','254','interfaces in ruby',3,'',7,1),(162,'2021-09-29 10:19:05.552947','253','sms mit python',3,'',7,1),(163,'2021-09-29 10:19:05.557721','252','commandline dialog',3,'',7,1),(164,'2021-09-29 10:19:05.564026','251','zu tun',3,'',7,1),(165,'2021-09-29 10:19:05.570908','250','how to use JavaScript with an MVC framework: angular and django',3,'',7,1),(166,'2021-09-29 10:19:05.575649','249','data science with ruby',3,'',7,1),(167,'2021-09-29 10:19:05.582902','248','raspi: netzwerk; inet über wlan, statische eth0',3,'',7,1),(168,'2021-09-29 10:19:05.587362','247','rpi-anwendung: innotex-klon',3,'',7,1),(169,'2021-09-29 10:19:05.594396','246','rpi installationslinks',3,'',7,1),(170,'2021-09-29 10:19:05.601813','245','django/python bücher',3,'',7,1),(171,'2021-09-29 10:19:05.609031','244','add an executable manually in linux',3,'',7,1),(172,'2021-09-29 10:19:05.615916','243','Reset forgotten/lost main user password in Linux Mint',3,'',7,1),(173,'2021-09-29 10:19:05.620570','242','How to create a CPU spike with a bash command',3,'',7,1),(174,'2021-09-29 10:19:05.627409','241','i3 6100 - system',3,'',7,1),(175,'2021-09-29 10:19:05.634588','240','mini-ITX gehäuse',3,'',7,1),(176,'2021-09-29 10:19:05.639269','239','mit curlftpfs festplatten über ftp mounten',3,'',7,1),(177,'2021-09-29 10:19:05.647897','238','150 Euro Multimedia-PC',3,'',7,1),(178,'2021-09-29 10:19:05.655257','237','Datenmengen speichern und verarbeiten: RRD-Tool, mit python',3,'',7,1),(179,'2021-09-29 10:19:05.662588','236','sonic pi: musik machen mit ruby-syntax',3,'',7,1),(180,'2021-09-29 10:19:05.666607','235','für jens',3,'',7,1),(181,'2021-09-29 10:19:05.673908','234','Remote Desktop/VNC mit remmina und tightvncserver',3,'',7,1),(182,'2021-09-29 10:19:05.678967','233','Procs und lambda in Ruby',3,'',7,1),(183,'2021-09-29 10:19:05.689620','232','ASCII-Logo/-Bild-Generatoren',3,'',7,1),(184,'2021-09-29 10:19:05.696702','231','python: cgi-skripte mit root-rechten ausführbar machen',3,'',7,1),(185,'2021-09-29 10:19:05.727872','230','ruby, rvm und cron: wrappers',3,'',7,1),(186,'2021-09-29 10:19:05.737522','229','sSMTP offiziell über Google mit einem Smarthost als Relay-Server.',3,'',7,1),(187,'2021-09-29 10:19:36.296234','228','restart phusion passenger to ensure reload of changed code',3,'',7,1),(188,'2021-09-29 10:19:36.301281','227','Python installieren unter Mint',3,'',7,1),(189,'2021-09-29 10:19:36.305281','226','problem mit VLC sound',3,'',7,1),(190,'2021-09-29 10:19:36.306924','225','python-wrapper to reddit\'s API',3,'',7,1),(191,'2021-09-29 10:19:36.308676','224','Qt-Tutorial (with Ruby or pyhton)',3,'',7,1),(192,'2021-09-29 10:19:36.316505','223','Secure Erasing: CLI-Tools',3,'',7,1),(193,'2021-09-29 10:19:36.323510','222','Problem mit sSMTP: Buchstabensalat dank United Internets custom header',3,'',7,1),(194,'2021-09-29 10:19:36.328136','221','yaml and ruby',3,'',7,1),(195,'2021-09-29 10:19:36.332154','220','CLI-email mit mutt',3,'',7,1),(196,'2021-09-29 10:19:36.337190','219','HowTo: Build a Beowulf cluster',3,'',7,1),(197,'2021-09-29 10:19:36.343983','218','python versions on debian',3,'',7,1),(198,'2021-09-29 10:19:36.347699','217','Raspbian: Anleitung, wie man Desktopumgebungen installiert. inkl Openbox-Setup!',3,'',7,1),(199,'2021-09-29 10:19:36.351669','216','git branching',3,'',7,1),(200,'2021-09-29 10:19:36.362939','215','memtest unter (debian-)linux',3,'',7,1),(201,'2021-09-29 10:19:36.367239','214','ableton live 9 on linux / wine',3,'',7,1),(202,'2021-09-29 10:19:36.371328','213','bunsen edit autostarted applications',3,'',7,1),(203,'2021-09-29 10:19:36.373048','212','bunsen: firefox statt iceweasel',3,'',7,1),(204,'2021-09-29 10:19:36.379844','211','bunsen with debian testing/stretch',3,'',7,1),(205,'2021-09-29 10:19:36.383811','210','headset',3,'',7,1),(206,'2021-09-29 10:19:36.385625','209','libreoffice: odt nach pdf konvertieren, und zwar in der CLI',3,'',7,1),(207,'2021-09-29 10:19:36.389316','208','borderlands 2: melee-zer0',3,'',7,1),(208,'2021-09-29 10:19:36.393635','207','mint-notizen (virtual machine)',3,'',7,1),(209,'2021-09-29 10:19:36.400478','206','Wahrscheinlichkeitsrechnung',3,'',7,1),(210,'2021-09-29 10:19:36.407132','205','Western Digital Festplatten: Problem mit IntelliPark beheben',3,'',7,1),(211,'2021-09-29 10:19:36.411222','204','Laufwerke automatisch über fstab mounten, ohne dass der boot scheitert wenn sie nicht da sind',3,'',7,1),(212,'2021-09-29 10:19:36.421724','203','Android-Smartphone als IP-Cam verwenden',3,'',7,1),(213,'2021-09-29 10:19:36.424538','202','privater Cloud-Speicher / Dateisynchronisierung',3,'',7,1),(214,'2021-09-29 10:19:36.429247','201','usb 3.0 karte auch unter linux',3,'',7,1),(215,'2021-09-29 10:19:36.430913','200','webserver/railsapp performance optimieren',3,'',7,1),(216,'2021-09-29 10:19:36.435100','199','rails server: phusion passenger',3,'',7,1),(217,'2021-09-29 10:19:36.439230','198','screentshot of sleepdata ',3,'',7,1),(218,'2021-09-29 10:19:36.443695','197','ssh dateiübertragung',3,'',7,1),(219,'2021-09-29 10:19:36.447977','196','set up a reverse proxy',3,'',7,1),(220,'2021-09-29 10:19:36.452368','195','texts for that english lesson',3,'',7,1),(221,'2021-09-29 10:19:36.456353','194','Alternatives to monkeypatching Ruby core classes',3,'',7,1),(222,'2021-09-29 10:19:36.460518','193','YAML ain\'t no Markup Language',3,'',7,1),(223,'2021-09-29 10:19:36.462186','192','SASS (Syntactically awesome CSS) und Rails',3,'',7,1),(224,'2021-09-29 10:19:36.466239','191','Ruby on Rails manuell unter Windows installieren',3,'',7,1),(225,'2021-09-29 10:19:36.470370','190','Hidden documentation: git commits with git blame',3,'',7,1),(226,'2021-09-29 10:19:36.478080','189','how to fix gitignore',3,'',7,1),(227,'2021-09-29 10:19:36.486135','188','simple search in rails',3,'',7,1),(228,'2021-09-29 10:19:36.490126','187','Code-Heroes to the Rescue! Ruby-Forum mit Leuten, die tatsächlich Fragen beantworten.',3,'',7,1),(229,'2021-09-29 10:19:36.494267','186','Rails: Destroying model',3,'',7,1),(230,'2021-09-29 10:19:36.495868','185','Datenvisualisierungs-Projekt',3,'',7,1),(231,'2021-09-29 10:19:36.499725','184','High-Power-LEDs',3,'',7,1),(232,'2021-09-29 10:19:36.504232','183','git force push',3,'',7,1),(233,'2021-09-29 10:19:36.511045','182','Ruby on Rails Tests with RSpec, maybe this works?',3,'',7,1),(234,'2021-09-29 10:19:36.515280','181','--delete-- GET YOUR FUCKING RUBY APP WORKING (clean install of ruby, rails, gems)',3,'',7,1),(235,'2021-09-29 10:19:36.519517','180','mit ruby gems arbeiten',3,'',7,1),(236,'2021-09-29 10:19:36.523682','179','bash error: You need to change your terminal emulator preferences to allow login shell.',3,'',7,1),(237,'2021-09-29 10:19:36.530743','178','rails-projekt auf neuem pc installieren\r\n',3,'',7,1),(238,'2021-09-29 10:19:36.532492','177','GET YOUR FUCKING RUBY APP WORKING (clean install of ruby, rails, gems)',3,'',7,1),(239,'2021-09-29 10:19:36.536357','176','bunsenlabs dual-monitor setup',3,'',7,1),(240,'2021-09-29 10:19:36.541493','175','Rails auf Raspberry Pi',3,'',7,1),(241,'2021-09-29 10:19:36.546358','174','Ruby on Rails: Phusion Passenger in Apache installieren',3,'',7,1),(242,'2021-09-29 10:19:36.550772','173','nach Hause /spawn point finden in Minecraft',3,'',7,1),(243,'2021-09-29 10:19:36.557616','172','git setup',3,'',7,1),(244,'2021-09-29 10:19:36.561665','171','ruby documentation: ri usage',3,'',7,1),(245,'2021-09-29 10:19:36.565870','170','setting up a new git repository',3,'',7,1),(246,'2021-09-29 10:19:36.573549','169','git revert / undo a commit, with a side of bash',3,'',7,1),(247,'2021-09-29 10:19:36.577313','168','Unit Testing with Ruby',3,'',7,1),(248,'2021-09-29 10:19:36.581397','167','atom editor',3,'',7,1),(249,'2021-09-29 10:19:36.583297','166','ruby webhosting',3,'',7,1),(250,'2021-09-29 10:19:36.588271','165','linux: set PATH for executables',3,'',7,1),(251,'2021-09-29 10:19:36.592456','164','git tutorial, git visualisierung, eigener git-server',3,'',7,1),(252,'2021-09-29 10:19:36.596822','163','Ruby Buchempfehlungen',3,'',7,1),(253,'2021-09-29 10:19:36.600997','162','Telegram-Plugin für Pidgin',3,'',7,1),(254,'2021-09-29 10:19:36.605571','161','gratis ebooks!',3,'',7,1),(255,'2021-09-29 10:19:36.610191','160','Data Visualization with JavaScript',3,'',7,1),(256,'2021-09-29 10:19:36.617161','159','email versenden in CLI, mit sSMTP',3,'',7,1),(257,'2021-09-29 10:19:36.619058','158','ruby installieren',3,'',7,1),(258,'2021-09-29 10:19:36.623301','157','öffentliche IP erfahren',3,'',7,1),(259,'2021-09-29 10:19:36.627412','156','GUI mit Ruby',3,'',7,1),(260,'2021-09-29 10:19:36.631450','155','.net mono (Open Source C#)',3,'',7,1),(261,'2021-09-29 10:19:36.635631','154','Windows 10 Nerver a.k.a. KB3035583 entfernen',3,'',7,1),(262,'2021-09-29 10:19:36.640179','153','coole telegram bots',3,'',7,1),(263,'2021-09-29 10:19:36.645822','152','r/homestuck ...zeug',3,'',7,1),(264,'2021-09-29 10:19:36.649996','151','Ruby GTK: Ruby mit GUI',3,'',7,1),(265,'2021-09-29 10:19:36.658059','150','Ruby-Script: Server-Adresse erreichbar?',3,'',7,1),(266,'2021-09-29 10:19:36.659869','149','schoppes bash-script',3,'',7,1),(267,'2021-09-29 10:19:36.666510','148','transmission directory',3,'',7,1),(268,'2021-09-29 10:19:36.668315','147','ruby on rails setup',3,'',7,1),(269,'2021-09-29 10:19:36.672828','146','ruby gem transmission-rss',3,'',7,1),(270,'2021-09-29 10:19:36.676883','145','automatisierter torrent-dl',3,'',7,1),(271,'2021-09-29 10:19:36.680888','144','Tutorials für Ruby on Rails',3,'',7,1),(272,'2021-09-29 10:19:36.685015','143','Tutorials für Django',3,'',7,1),(273,'2021-09-29 10:19:36.689327','142','Shell-Scripting mit PHP',3,'',7,1),(274,'2021-09-29 10:19:36.696347','141','Shell-Befehle mit Ruby ausführen',3,'',7,1),(275,'2021-09-29 10:19:36.701385','140','Everyday Scripting ... with Node?',3,'',7,1),(276,'2021-09-29 10:19:36.703560','139','Android- und iOS-Apps mit Skriptsprachen',3,'',7,1),(277,'2021-09-29 10:19:36.708051','138','Everyday Scripting with Python',3,'',7,1),(278,'2021-09-29 10:19:36.709815','137','Everyday Scripting with Ruby',3,'',7,1),(279,'2021-09-29 10:19:36.714135','136','PHP vs Ruby',3,'',7,1),(280,'2021-09-29 10:19:36.724846','135','benchmarks for ruby on rails',3,'',7,1),(281,'2021-09-29 10:19:36.726759','134','frameworks',3,'',7,1),(282,'2021-09-29 10:19:36.741881','133','Node vs Ruby on Rails',3,'',7,1),(283,'2021-09-29 10:19:36.744059','132','Node.js HowTo: Meteor',3,'',7,1),(284,'2021-09-29 10:19:36.748255','131','Ruby on Rails Tutorial(s)',3,'',7,1),(285,'2021-09-29 10:19:36.749897','130','Node.js HowTo: HTTP-Server, File-Server',3,'',7,1),(286,'2021-09-29 10:19:36.756402','129','Ruby-on-Rails Heimserver Tutorials',3,'',7,1),(287,'2021-09-29 10:20:12.569242','128','Code::Blocks einrichtung',3,'',7,1),(288,'2021-09-29 10:20:12.571285','127','Minecraft Server',3,'',7,1),(289,'2021-09-29 10:20:12.578168','124','glassfish auf raspberry pi installieren',3,'',7,1),(290,'2021-09-29 10:20:12.581364','123','Glassfish / Java Monitoring / Performance Tuning',3,'',7,1),(291,'2021-09-29 10:20:12.588460','122','Minecraft',3,'',7,1),(292,'2021-09-29 10:20:12.595547','121','Glassfish Deployment: Database Connection',3,'',7,1),(293,'2021-09-29 10:20:12.600060','120','Tomcat Deployment',3,'',7,1),(294,'2021-09-29 10:20:12.607184','119','Dezimal-Formatierung / Runden in Java',3,'',7,1),(295,'2021-09-29 10:20:12.614237','118','UML',3,'',7,1),(296,'2021-09-29 10:20:12.621408','117','MySQL-Datenbank in Eclipse-JEE',3,'',7,1),(297,'2021-09-29 10:20:12.628596','116','Datenbank-Dump in Netbeans',3,'',7,1),(298,'2021-09-29 10:20:12.633068','115','Anleitung: JEE Webanwendung mit Netbeans',3,'',7,1),(299,'2021-09-29 10:20:12.639928','114','Java-Datenbanken mit Derby in Eclipse',3,'',7,1),(300,'2021-09-29 10:20:12.647037','113','Ideen für Programmier-Projekte',3,'',7,1),(301,'2021-09-29 10:20:12.653891','112','interessante unternehmen',3,'',7,1),(302,'2021-09-29 10:20:12.658384','111','gratis-Webhosting für Java',3,'',7,1),(303,'2021-09-29 10:20:12.665427','110','Eclipse: Rechtschreibprüfung ausschalten',3,'',7,1),(304,'2021-09-29 10:20:12.670116','109','Eclipse Luna: Schriftgröße ändern',3,'',7,1),(305,'2021-09-29 10:20:12.676363','108','reaper on linux',3,'',7,1),(306,'2021-09-29 10:20:12.680921','107','bootable USB unter Debian',3,'',7,1),(307,'2021-09-29 10:20:12.688175','106','Texthintergrund Idea/Android Studio',3,'',7,1),(308,'2021-09-29 10:20:12.691527','105','Android Studio Einstellungen',3,'',7,1),(309,'2021-09-29 10:20:12.698698','104','Netbeans Optionen',3,'',7,1),(310,'2021-09-29 10:20:12.705608','103','Roll dein eigenes Linux!',3,'',7,1),(311,'2021-09-29 10:20:12.709666','102','linux md5 sha256 checksums',3,'',7,1),(312,'2021-09-29 10:20:12.713555','101','Run .bin file in Debian',3,'',7,1),(313,'2021-09-29 10:20:12.715644','100','Bubble Sort-Algorythmus',3,'',7,1),(314,'2021-09-29 10:20:12.724200','99','Useful PHP Components, Techniques and Tutorials\r\n\r\n',3,'',7,1),(315,'2021-09-29 10:20:12.731627','98','Wifi neustarten in Ubuntu 12',3,'',7,1),(316,'2021-09-29 10:20:12.736009','97','save file locations in spielen',3,'',7,1),(317,'2021-09-29 10:20:12.740665','96','PHP: Daten aus Logdatei lesen',3,'',7,1),(318,'2021-09-29 10:20:12.745322','95','Agar.io ',3,'',7,1),(319,'2021-09-29 10:20:12.749610','94','equals-Methode',3,'',7,1),(320,'2021-09-29 10:20:12.768210','93','javascript spielerei',3,'',7,1),(321,'2021-09-29 10:20:12.777525','92','Ajax Form Submit Example',3,'',7,1),(322,'2021-09-29 10:20:12.781588','91','php session Dauer',3,'',7,1),(323,'2021-09-29 10:20:12.788445','90','bunsenlabs notizen',3,'',7,1),(324,'2021-09-29 10:20:12.796432','89','website frameworks wikipedia',3,'',7,1),(325,'2021-09-29 10:20:12.801105','88','Fix für niedrige Download-Geschwindigkeit bei Steam unter bunsenlabs',3,'',7,1),(326,'2021-09-29 10:20:12.807936','87','Using the Keyword super',3,'',7,1),(327,'2021-09-29 10:20:12.812639','86','Ideen für Programmier-Projekte',3,'',7,1),(328,'2021-09-29 10:20:12.816750','85','Bunsenlabs: Themes, Wallpaper',3,'',7,1),(329,'2021-09-29 10:20:12.823940','84','bunsenlabs audio not working',3,'',7,1),(330,'2021-09-29 10:20:12.831092','83','php session Dauer anpassen / session expiring too early',3,'',7,1),(331,'2021-09-29 10:20:12.838133','82','Gedit Editor Split View',3,'',7,1),(332,'2021-09-29 10:20:12.843175','81','Kiosk am Schapdamm Öffnungszeiten',3,'',7,1),(333,'2021-09-29 10:20:12.847325','80','debian server linksammlung',3,'',7,1),(334,'2021-09-29 10:20:12.855909','79','Verstecken der Statuszeile von Eclipse',3,'',7,1),(335,'2021-09-29 10:20:12.861579','78','linefeed problem: dos2unix anleitung',3,'',7,1),(336,'2021-09-29 10:20:12.868967','77','linefeed problem: dos2unix anleitung',3,'',7,1),(337,'2021-09-29 10:20:12.876849','76','Eclipse - how to count lines of code\r\n',3,'',7,1),(338,'2021-09-29 10:20:12.881022','75','executable .jar-File unter Windows',3,'',7,1),(339,'2021-09-29 10:20:12.885515','74','java.lang.NullPointerException - Common cause of NullPointerException in Java ',3,'',7,1),(340,'2021-09-29 10:20:12.889971','73','php, sprintf analog zu printf',3,'',7,1),(341,'2021-09-29 10:20:12.897061','72','selbstgeschriebene Funktionen in PHP',3,'',7,1),(342,'2021-09-29 10:20:12.903056','71','Replace URLs in text with HTML links',3,'',7,1),(343,'2021-09-29 10:20:12.910132','70','tutorialspoint',3,'',7,1),(344,'2021-09-29 10:20:12.914359','69','tutorialspoint java',3,'',7,1),(345,'2021-09-29 10:20:12.921500','68','Eloquent Javascript, freies EBook über JavaScript (und ein wenig Node)',3,'',7,1),(346,'2021-09-29 10:20:12.926327','67','PHP vs. Node.js (technischer Artikel)',3,'',7,1),(347,'2021-09-29 10:20:12.933568','66','PHP vs. Node.js',3,'',7,1),(348,'2021-09-29 10:20:12.940769','65','Java vs. Node.js: An epic battle for developer mind share',3,'',7,1),(349,'2021-09-29 10:20:12.948077','64','To Node.js Or Not To Node.js',3,'',7,1),(350,'2021-09-29 10:20:12.952375','63','login-seite',3,'',7,1),(351,'2021-09-29 10:20:12.956758','58','CSS centering horizontally and vertically with FLEX',3,'',7,1),(352,'2021-09-29 10:20:12.963838','57','kiosk am schapdamm',3,'',7,1),(353,'2021-09-29 10:20:12.970997','52','Eric Meyer\'s CSS Reset',3,'',7,1),(354,'2021-09-29 10:20:12.977832','51','mysql_real_escape_string() richtig implementieren',3,'',7,1),(355,'2021-09-29 10:20:12.984840','48','Cannot send session cookie - headers already sent',3,'',7,1),(356,'2021-09-29 10:20:12.991841','45','Fehlermeldung bei mysql_real_escape_string: Access denied for user \'\'@\'localhost\' (using password: NO)',3,'',7,1),(357,'2021-09-29 10:20:12.996567','29','You have an error in your SQL syntax / mysqli_real_escape_string',3,'',7,1),(358,'2021-09-29 10:20:13.003867','28','Blocking Brute Force in PHP Website',3,'',7,1),(359,'2021-09-29 10:20:13.006000','22','Linux dd',3,'',7,1),(360,'2021-09-29 10:20:13.013080','18','Format a Decimal Number Using Scientific Notation in Java ',3,'',7,1),(361,'2021-09-29 10:21:58.779741','408','apt: allow unauthenticated repositories',3,'',7,1),(362,'2021-09-29 10:21:58.783931','402','how to use cinnamon and i3wm',3,'',7,1),(363,'2021-09-29 10:21:58.791167','401','install GNU IceCat',3,'',7,1),(364,'2021-09-29 10:21:58.795500','396','how to respond to \"I have nothing to hide\"',3,'',7,1),(365,'2021-09-29 10:21:58.799973','393','firefox: solve problem with unreadable input boxes with dark GTK-themes',3,'',7,1),(366,'2021-09-29 10:21:58.804250','391','solve pgp-issue, use keyring in profanity',3,'',7,1),(367,'2021-09-29 10:21:58.811038','389','gedit shortcuts',3,'',7,1),(368,'2021-09-29 10:21:58.817740','378','Delayed Sleep Phase Disorder',3,'',7,1),(369,'2021-09-29 10:21:58.822302','375','atom editor cheat sheet',3,'',7,1),(370,'2021-09-29 10:21:58.829166','366','ARM-server sachen',3,'',7,1),(371,'2021-09-29 10:21:58.836060','355','Hornet SSH',3,'',7,1),(372,'2021-09-29 10:21:58.840128','350','git (nach tim)',3,'',7,1),(373,'2021-09-29 10:21:58.844399','349','making Angular safe for minification',3,'',7,1),(374,'2021-09-29 10:21:58.846115','345','minecraft to-do',3,'',7,1),(375,'2021-09-29 10:21:58.850736','335','misc. reading documentation',3,'',7,1),(376,'2021-09-29 10:21:58.855024','331','i3-gaps .deb tool',3,'',7,1),(377,'2021-09-29 10:21:58.859215','330','gtk-theme switcher for debian',3,'',7,1),(378,'2021-09-29 10:21:58.863562','329','i3-config für bunsenlabs stretch',3,'',7,1);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(7,'artikel','artikel'),(4,'auth','group'),(2,'auth','permission'),(3,'auth','user'),(5,'contenttypes','contenttype'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2018-01-19 23:58:46.398853'),(2,'auth','0001_initial','2018-01-19 23:58:50.285065'),(3,'admin','0001_initial','2018-01-19 23:58:50.658257'),(4,'admin','0002_logentry_remove_auto_add','2018-01-19 23:58:50.687870'),(5,'contenttypes','0002_remove_content_type_name','2018-01-19 23:58:50.899769'),(6,'auth','0002_alter_permission_name_max_length','2018-01-19 23:58:51.052418'),(7,'auth','0003_alter_user_email_max_length','2018-01-19 23:58:51.202803'),(8,'auth','0004_alter_user_username_opts','2018-01-19 23:58:51.226182'),(9,'auth','0005_alter_user_last_login_null','2018-01-19 23:58:51.332944'),(10,'auth','0006_require_contenttypes_0002','2018-01-19 23:58:51.341558'),(11,'auth','0007_alter_validators_add_error_messages','2018-01-19 23:58:51.392744'),(12,'auth','0008_alter_user_username_max_length','2018-01-19 23:58:51.692021'),(13,'sessions','0001_initial','2018-01-19 23:58:51.819308'),(14,'artikel','0001_initial','2018-01-26 09:47:54.870940'),(15,'artikel','0002_auto_20180122_2226','2018-01-26 09:47:54.906452'),(16,'artikel','0002_auto_20180126_0952','2018-01-26 09:54:32.212404'),(17,'artikel','0003_artikel_owner','2021-03-13 13:01:04.386381'),(18,'artikel','0004_auto_20180405_1050','2021-03-13 13:07:19.105428'),(19,'admin','0003_logentry_add_action_flag_choices','2021-09-16 20:23:56.321217'),(20,'auth','0009_alter_user_last_name_max_length','2021-09-16 20:23:57.431690'),(21,'auth','0010_alter_group_name_max_length','2021-09-16 20:23:57.512600'),(22,'auth','0011_update_proxy_permissions','2021-09-16 20:23:57.555260'),(23,'auth','0012_alter_user_first_name_max_length','2021-09-16 20:23:58.321167');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('03ddkx8szfuqm2exrhua29noiyob95l0','N2M1ODMyYmU5ODNiNzEwNzQ1ZmUyZjA2NGZjOTI1OWJiNmRmOGNkOTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI2OGNlYmVhMWI2NWIwMTg2YzE5MjA2ZmE0NjFhMzM5YzMzZDQ4OTE3In0=','2021-05-07 03:24:42.011689'),('0gnvqssn5tl228y3awbim72yren7v7mg','N2M1ODMyYmU5ODNiNzEwNzQ1ZmUyZjA2NGZjOTI1OWJiNmRmOGNkOTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI2OGNlYmVhMWI2NWIwMTg2YzE5MjA2ZmE0NjFhMzM5YzMzZDQ4OTE3In0=','2021-03-29 16:36:58.412235'),('2oswt19mues9k8oq237dqds1ddwo0mzs','N2M1ODMyYmU5ODNiNzEwNzQ1ZmUyZjA2NGZjOTI1OWJiNmRmOGNkOTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI2OGNlYmVhMWI2NWIwMTg2YzE5MjA2ZmE0NjFhMzM5YzMzZDQ4OTE3In0=','2021-04-16 07:09:34.937665'),('4ctgs7ghfy48c1uhwas75go40o8hme5j','.eJxVjMsOwiAQRf-FtSG8Cy7d-w1kmAGpGkhKuzL-uzbpQrf3nHNfLMK21riNvMSZ2JlJdvrdEuAjtx3QHdqtc-xtXebEd4UfdPBrp_y8HO7fQYVRv3VGUEkHokKghUCnUGsHLlNRhgyQ9cl4mADDVLxQEBTalISU0hqnkb0_Efw4dg:1mVWdY:c-5K5_-_sngJMvtjUel893QvceLemBTZ6hB6CYOXC-o','2021-10-13 10:16:28.617695'),('4k8o5aj2hss8kbyybkkqyyxv2rms3a31','N2M1ODMyYmU5ODNiNzEwNzQ1ZmUyZjA2NGZjOTI1OWJiNmRmOGNkOTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI2OGNlYmVhMWI2NWIwMTg2YzE5MjA2ZmE0NjFhMzM5YzMzZDQ4OTE3In0=','2021-05-13 19:01:15.535498'),('4sfvkq25etsmr4xqqwhgig3ncz8afe36','N2M1ODMyYmU5ODNiNzEwNzQ1ZmUyZjA2NGZjOTI1OWJiNmRmOGNkOTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI2OGNlYmVhMWI2NWIwMTg2YzE5MjA2ZmE0NjFhMzM5YzMzZDQ4OTE3In0=','2021-04-19 14:51:22.886915'),('50qcgdc2b0ygmsmx3i95it0ingzjyvc8','N2M1ODMyYmU5ODNiNzEwNzQ1ZmUyZjA2NGZjOTI1OWJiNmRmOGNkOTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI2OGNlYmVhMWI2NWIwMTg2YzE5MjA2ZmE0NjFhMzM5YzMzZDQ4OTE3In0=','2021-06-04 04:08:22.725925'),('5a9z7iss88qt872v5lqq0ihfjcjysd0u','N2M1ODMyYmU5ODNiNzEwNzQ1ZmUyZjA2NGZjOTI1OWJiNmRmOGNkOTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI2OGNlYmVhMWI2NWIwMTg2YzE5MjA2ZmE0NjFhMzM5YzMzZDQ4OTE3In0=','2021-04-26 14:28:18.861471'),('5rgexws467sfd28y38uch3scftfd7u0i','N2M1ODMyYmU5ODNiNzEwNzQ1ZmUyZjA2NGZjOTI1OWJiNmRmOGNkOTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI2OGNlYmVhMWI2NWIwMTg2YzE5MjA2ZmE0NjFhMzM5YzMzZDQ4OTE3In0=','2021-09-01 13:56:56.565355'),('7vfl3pkmkxomu8i9jm2wfd60r0ki9z7g','ZThjNGYwMDljZDU2NmU0YjliN2E0NmM5M2JlODNhZmU1M2Y4YzA5YTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI3MzhmYTZhYWU1ZDM2ZmQ3NjViMTY2NTI5NjViZGU1NzFjZGY2ODdkIn0=','2020-02-24 17:54:06.149704'),('aeegpr29ppc7bwlyq348z2x9f5dkx2fw','N2M1ODMyYmU5ODNiNzEwNzQ1ZmUyZjA2NGZjOTI1OWJiNmRmOGNkOTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI2OGNlYmVhMWI2NWIwMTg2YzE5MjA2ZmE0NjFhMzM5YzMzZDQ4OTE3In0=','2021-03-27 13:11:22.033591'),('cq488rv1updsna2qq6ifext3cig7gokt','N2M1ODMyYmU5ODNiNzEwNzQ1ZmUyZjA2NGZjOTI1OWJiNmRmOGNkOTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI2OGNlYmVhMWI2NWIwMTg2YzE5MjA2ZmE0NjFhMzM5YzMzZDQ4OTE3In0=','2021-04-03 23:02:28.949395'),('g5k6obm8j06du1yyiiflt2p2vwg39fzb','N2M1ODMyYmU5ODNiNzEwNzQ1ZmUyZjA2NGZjOTI1OWJiNmRmOGNkOTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI2OGNlYmVhMWI2NWIwMTg2YzE5MjA2ZmE0NjFhMzM5YzMzZDQ4OTE3In0=','2021-03-29 18:31:01.411202'),('kk7vggadn9pftu4itf8u85v4tv394yfy','N2M1ODMyYmU5ODNiNzEwNzQ1ZmUyZjA2NGZjOTI1OWJiNmRmOGNkOTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI2OGNlYmVhMWI2NWIwMTg2YzE5MjA2ZmE0NjFhMzM5YzMzZDQ4OTE3In0=','2021-06-25 16:21:02.299219'),('m61rzr8j0m0frv326brn56pf9d0e0hxb','N2M1ODMyYmU5ODNiNzEwNzQ1ZmUyZjA2NGZjOTI1OWJiNmRmOGNkOTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI2OGNlYmVhMWI2NWIwMTg2YzE5MjA2ZmE0NjFhMzM5YzMzZDQ4OTE3In0=','2021-06-08 03:16:05.275611'),('pffidtdpd2ytxvvbzxl229ll3fa1ye4f','N2M1ODMyYmU5ODNiNzEwNzQ1ZmUyZjA2NGZjOTI1OWJiNmRmOGNkOTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI2OGNlYmVhMWI2NWIwMTg2YzE5MjA2ZmE0NjFhMzM5YzMzZDQ4OTE3In0=','2021-04-04 04:40:49.697699'),('qs2ndgwfpw5sqd7pzvwri7a09uvfuvbk','N2M1ODMyYmU5ODNiNzEwNzQ1ZmUyZjA2NGZjOTI1OWJiNmRmOGNkOTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI2OGNlYmVhMWI2NWIwMTg2YzE5MjA2ZmE0NjFhMzM5YzMzZDQ4OTE3In0=','2021-08-25 20:31:17.413771'),('rakhnrqj14m1hvt9z4li4wdgm067weo6','ODM4ZTg0YjY0MzRmOTk4ZTAyYWI1MDczODEyY2EwZjJlZjhjMzYxNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNjhjZWJlYTFiNjViMDE4NmMxOTIwNmZhNDYxYTMzOWMzM2Q0ODkxNyIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=','2021-03-26 14:03:25.176357'),('sl4jesw1yciax38veen8jw9ocyepdu74','YWU5ZDlkMjBhZmYyYTU1ZGJjYjU0NGU1YjkxNmRhNzc2MTU0YTA0NTp7Il9hdXRoX3VzZXJfaGFzaCI6IjczOGZhNmFhZTVkMzZmZDc2NWIxNjY1Mjk2NWJkZTU3MWNkZjY4N2QiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-02-03 00:09:10.966888'),('t9xr32zo5uz05bxds10we7kydukdckgt','ZThjNGYwMDljZDU2NmU0YjliN2E0NmM5M2JlODNhZmU1M2Y4YzA5YTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI3MzhmYTZhYWU1ZDM2ZmQ3NjViMTY2NTI5NjViZGU1NzFjZGY2ODdkIn0=','2020-01-13 00:51:24.378030'),('uc676nr9h6sxfykmqzgdc5e8d20mcyki','N2M1ODMyYmU5ODNiNzEwNzQ1ZmUyZjA2NGZjOTI1OWJiNmRmOGNkOTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI2OGNlYmVhMWI2NWIwMTg2YzE5MjA2ZmE0NjFhMzM5YzMzZDQ4OTE3In0=','2021-05-07 20:48:10.097266'),('v54bi8jogpgimftavwr4v5xul2dr7xi9','.eJxVjMsOwiAQRf-FtSEjb1y67zeQgQGpGkhKuzL-uzbpQrf3nHNfLOC21rCNvISZ2IUJdvrdIqZHbjugO7Zb56m3dZkj3xV-0MGnTvl5Pdy_g4qjfuuM2jprMRkjvACRjQfhPRYk74jOkIX0ipRWCciilZpkhAI6OnJYgL0_0sM3qg:1mQuig:wKLkRFG3gwGh_qP_BB4HSnHGtoJGR9CsldIoSD8w26w','2021-09-30 16:58:42.927676'),('wlxh7k0sv4z33lgk83br6lpz5jkrnory','N2M1ODMyYmU5ODNiNzEwNzQ1ZmUyZjA2NGZjOTI1OWJiNmRmOGNkOTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI2OGNlYmVhMWI2NWIwMTg2YzE5MjA2ZmE0NjFhMzM5YzMzZDQ4OTE3In0=','2021-07-29 10:52:58.549141'),('xn2eoixmb2v4ngdoy6fsg1h7ek8yr5za','YWU5ZDlkMjBhZmYyYTU1ZGJjYjU0NGU1YjkxNmRhNzc2MTU0YTA0NTp7Il9hdXRoX3VzZXJfaGFzaCI6IjczOGZhNmFhZTVkMzZmZDc2NWIxNjY1Mjk2NWJkZTU3MWNkZjY4N2QiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2020-01-05 03:37:23.008461'),('yh5htxt76qfl5kohtlhexibmkf63w6zk','N2M1ODMyYmU5ODNiNzEwNzQ1ZmUyZjA2NGZjOTI1OWJiNmRmOGNkOTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI2OGNlYmVhMWI2NWIwMTg2YzE5MjA2ZmE0NjFhMzM5YzMzZDQ4OTE3In0=','2021-05-18 00:15:36.694531');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nutzerdaten`
--

DROP TABLE IF EXISTS `nutzerdaten`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nutzerdaten` (
  `nutzer_id` int(2) NOT NULL AUTO_INCREMENT,
  `nutzer` varchar(128) NOT NULL,
  `pwhash` varchar(128) NOT NULL,
  `versuche` varchar(20) NOT NULL,
  PRIMARY KEY (`nutzer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nutzerdaten`
--

LOCK TABLES `nutzerdaten` WRITE;
/*!40000 ALTER TABLE `nutzerdaten` DISABLE KEYS */;
INSERT INTO `nutzerdaten` VALUES (3,'rynnstein','$2y$10$ome69YK0DYZUcjnja55o2e8ieI0P7rHabDmMQVScdM1ScNKMUTqra','0');
/*!40000 ALTER TABLE `nutzerdaten` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__bookmark`
--

DROP TABLE IF EXISTS `pma__bookmark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__bookmark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dbase` varchar(255) COLLATE utf8mb3_bin NOT NULL DEFAULT '',
  `user` varchar(255) COLLATE utf8mb3_bin NOT NULL DEFAULT '',
  `label` varchar(255) CHARACTER SET utf8mb3 NOT NULL DEFAULT '',
  `query` text COLLATE utf8mb3_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin COMMENT='Bookmarks';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__bookmark`
--

LOCK TABLES `pma__bookmark` WRITE;
/*!40000 ALTER TABLE `pma__bookmark` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__bookmark` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__central_columns`
--

DROP TABLE IF EXISTS `pma__central_columns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__central_columns` (
  `db_name` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `col_name` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `col_type` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `col_length` text COLLATE utf8mb3_bin DEFAULT NULL,
  `col_collation` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `col_isNull` tinyint(1) NOT NULL,
  `col_extra` varchar(255) COLLATE utf8mb3_bin DEFAULT '',
  `col_default` text COLLATE utf8mb3_bin DEFAULT NULL,
  PRIMARY KEY (`db_name`,`col_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin COMMENT='Central list of columns';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__central_columns`
--

LOCK TABLES `pma__central_columns` WRITE;
/*!40000 ALTER TABLE `pma__central_columns` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__central_columns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__column_info`
--

DROP TABLE IF EXISTS `pma__column_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__column_info` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `db_name` varchar(64) COLLATE utf8mb3_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8mb3_bin NOT NULL DEFAULT '',
  `column_name` varchar(64) COLLATE utf8mb3_bin NOT NULL DEFAULT '',
  `comment` varchar(255) CHARACTER SET utf8mb3 NOT NULL DEFAULT '',
  `mimetype` varchar(255) CHARACTER SET utf8mb3 NOT NULL DEFAULT '',
  `transformation` varchar(255) COLLATE utf8mb3_bin NOT NULL DEFAULT '',
  `transformation_options` varchar(255) COLLATE utf8mb3_bin NOT NULL DEFAULT '',
  `input_transformation` varchar(255) COLLATE utf8mb3_bin NOT NULL DEFAULT '',
  `input_transformation_options` varchar(255) COLLATE utf8mb3_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `db_name` (`db_name`,`table_name`,`column_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin COMMENT='Column information for phpMyAdmin';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__column_info`
--

LOCK TABLES `pma__column_info` WRITE;
/*!40000 ALTER TABLE `pma__column_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__column_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__designer_settings`
--

DROP TABLE IF EXISTS `pma__designer_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__designer_settings` (
  `username` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `settings_data` text COLLATE utf8mb3_bin NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin COMMENT='Settings related to Designer';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__designer_settings`
--

LOCK TABLES `pma__designer_settings` WRITE;
/*!40000 ALTER TABLE `pma__designer_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__designer_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__export_templates`
--

DROP TABLE IF EXISTS `pma__export_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__export_templates` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `export_type` varchar(10) COLLATE utf8mb3_bin NOT NULL,
  `template_name` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `template_data` text COLLATE utf8mb3_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_user_type_template` (`username`,`export_type`,`template_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin COMMENT='Saved export templates';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__export_templates`
--

LOCK TABLES `pma__export_templates` WRITE;
/*!40000 ALTER TABLE `pma__export_templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__export_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__favorite`
--

DROP TABLE IF EXISTS `pma__favorite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__favorite` (
  `username` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `tables` text COLLATE utf8mb3_bin NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin COMMENT='Favorite tables';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__favorite`
--

LOCK TABLES `pma__favorite` WRITE;
/*!40000 ALTER TABLE `pma__favorite` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__favorite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__history`
--

DROP TABLE IF EXISTS `pma__history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__history` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) COLLATE utf8mb3_bin NOT NULL DEFAULT '',
  `db` varchar(64) COLLATE utf8mb3_bin NOT NULL DEFAULT '',
  `table` varchar(64) COLLATE utf8mb3_bin NOT NULL DEFAULT '',
  `timevalue` timestamp NOT NULL DEFAULT current_timestamp(),
  `sqlquery` text COLLATE utf8mb3_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `username` (`username`,`db`,`table`,`timevalue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin COMMENT='SQL history for phpMyAdmin';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__history`
--

LOCK TABLES `pma__history` WRITE;
/*!40000 ALTER TABLE `pma__history` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__navigationhiding`
--

DROP TABLE IF EXISTS `pma__navigationhiding`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__navigationhiding` (
  `username` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `item_name` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `item_type` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  PRIMARY KEY (`username`,`item_name`,`item_type`,`db_name`,`table_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin COMMENT='Hidden items of navigation tree';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__navigationhiding`
--

LOCK TABLES `pma__navigationhiding` WRITE;
/*!40000 ALTER TABLE `pma__navigationhiding` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__navigationhiding` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__pdf_pages`
--

DROP TABLE IF EXISTS `pma__pdf_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__pdf_pages` (
  `db_name` varchar(64) COLLATE utf8mb3_bin NOT NULL DEFAULT '',
  `page_nr` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_descr` varchar(50) CHARACTER SET utf8mb3 NOT NULL DEFAULT '',
  PRIMARY KEY (`page_nr`),
  KEY `db_name` (`db_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin COMMENT='PDF relation pages for phpMyAdmin';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__pdf_pages`
--

LOCK TABLES `pma__pdf_pages` WRITE;
/*!40000 ALTER TABLE `pma__pdf_pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__pdf_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__recent`
--

DROP TABLE IF EXISTS `pma__recent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__recent` (
  `username` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `tables` text COLLATE utf8mb3_bin NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin COMMENT='Recently accessed tables';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__recent`
--

LOCK TABLES `pma__recent` WRITE;
/*!40000 ALTER TABLE `pma__recent` DISABLE KEYS */;
INSERT INTO `pma__recent` VALUES ('phpmyadmin','[{\"db\":\"phpmyadmin\",\"table\":\"nutzerdaten\"},{\"db\":\"phpmyadmin\",\"table\":\"artikel\"}]'),('root','[{\"db\":\"phpmyadmin\",\"table\":\"artikel\"}]');
/*!40000 ALTER TABLE `pma__recent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__relation`
--

DROP TABLE IF EXISTS `pma__relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__relation` (
  `master_db` varchar(64) COLLATE utf8mb3_bin NOT NULL DEFAULT '',
  `master_table` varchar(64) COLLATE utf8mb3_bin NOT NULL DEFAULT '',
  `master_field` varchar(64) COLLATE utf8mb3_bin NOT NULL DEFAULT '',
  `foreign_db` varchar(64) COLLATE utf8mb3_bin NOT NULL DEFAULT '',
  `foreign_table` varchar(64) COLLATE utf8mb3_bin NOT NULL DEFAULT '',
  `foreign_field` varchar(64) COLLATE utf8mb3_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`master_db`,`master_table`,`master_field`),
  KEY `foreign_field` (`foreign_db`,`foreign_table`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin COMMENT='Relation table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__relation`
--

LOCK TABLES `pma__relation` WRITE;
/*!40000 ALTER TABLE `pma__relation` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__savedsearches`
--

DROP TABLE IF EXISTS `pma__savedsearches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__savedsearches` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) COLLATE utf8mb3_bin NOT NULL DEFAULT '',
  `db_name` varchar(64) COLLATE utf8mb3_bin NOT NULL DEFAULT '',
  `search_name` varchar(64) COLLATE utf8mb3_bin NOT NULL DEFAULT '',
  `search_data` text COLLATE utf8mb3_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_savedsearches_username_dbname` (`username`,`db_name`,`search_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin COMMENT='Saved searches';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__savedsearches`
--

LOCK TABLES `pma__savedsearches` WRITE;
/*!40000 ALTER TABLE `pma__savedsearches` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__savedsearches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__table_coords`
--

DROP TABLE IF EXISTS `pma__table_coords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__table_coords` (
  `db_name` varchar(64) COLLATE utf8mb3_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8mb3_bin NOT NULL DEFAULT '',
  `pdf_page_number` int(11) NOT NULL DEFAULT 0,
  `x` float unsigned NOT NULL DEFAULT 0,
  `y` float unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`db_name`,`table_name`,`pdf_page_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin COMMENT='Table coordinates for phpMyAdmin PDF output';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__table_coords`
--

LOCK TABLES `pma__table_coords` WRITE;
/*!40000 ALTER TABLE `pma__table_coords` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__table_coords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__table_info`
--

DROP TABLE IF EXISTS `pma__table_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__table_info` (
  `db_name` varchar(64) COLLATE utf8mb3_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8mb3_bin NOT NULL DEFAULT '',
  `display_field` varchar(64) COLLATE utf8mb3_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`db_name`,`table_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin COMMENT='Table information for phpMyAdmin';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__table_info`
--

LOCK TABLES `pma__table_info` WRITE;
/*!40000 ALTER TABLE `pma__table_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__table_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__table_uiprefs`
--

DROP TABLE IF EXISTS `pma__table_uiprefs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__table_uiprefs` (
  `username` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `prefs` text COLLATE utf8mb3_bin NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`username`,`db_name`,`table_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin COMMENT='Tables'' UI preferences';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__table_uiprefs`
--

LOCK TABLES `pma__table_uiprefs` WRITE;
/*!40000 ALTER TABLE `pma__table_uiprefs` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__table_uiprefs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__tracking`
--

DROP TABLE IF EXISTS `pma__tracking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__tracking` (
  `db_name` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `version` int(10) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `schema_snapshot` text COLLATE utf8mb3_bin NOT NULL,
  `schema_sql` text COLLATE utf8mb3_bin DEFAULT NULL,
  `data_sql` longtext COLLATE utf8mb3_bin DEFAULT NULL,
  `tracking` set('UPDATE','REPLACE','INSERT','DELETE','TRUNCATE','CREATE DATABASE','ALTER DATABASE','DROP DATABASE','CREATE TABLE','ALTER TABLE','RENAME TABLE','DROP TABLE','CREATE INDEX','DROP INDEX','CREATE VIEW','ALTER VIEW','DROP VIEW') COLLATE utf8mb3_bin DEFAULT NULL,
  `tracking_active` int(1) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`db_name`,`table_name`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin COMMENT='Database changes tracking for phpMyAdmin';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__tracking`
--

LOCK TABLES `pma__tracking` WRITE;
/*!40000 ALTER TABLE `pma__tracking` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__tracking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__userconfig`
--

DROP TABLE IF EXISTS `pma__userconfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__userconfig` (
  `username` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `timevalue` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `config_data` text COLLATE utf8mb3_bin NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin COMMENT='User preferences storage for phpMyAdmin';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__userconfig`
--

LOCK TABLES `pma__userconfig` WRITE;
/*!40000 ALTER TABLE `pma__userconfig` DISABLE KEYS */;
INSERT INTO `pma__userconfig` VALUES ('phpmyadmin','2018-01-09 13:42:52','{\"collation_connection\":\"utf8mb4_unicode_ci\"}'),('root','2018-01-09 13:45:53','{\"collation_connection\":\"utf8mb4_unicode_ci\"}');
/*!40000 ALTER TABLE `pma__userconfig` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__usergroups`
--

DROP TABLE IF EXISTS `pma__usergroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__usergroups` (
  `usergroup` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `tab` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `allowed` enum('Y','N') COLLATE utf8mb3_bin NOT NULL DEFAULT 'N',
  PRIMARY KEY (`usergroup`,`tab`,`allowed`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin COMMENT='User groups with configured menu items';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__usergroups`
--

LOCK TABLES `pma__usergroups` WRITE;
/*!40000 ALTER TABLE `pma__usergroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__usergroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pma__users`
--

DROP TABLE IF EXISTS `pma__users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pma__users` (
  `username` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  `usergroup` varchar(64) COLLATE utf8mb3_bin NOT NULL,
  PRIMARY KEY (`username`,`usergroup`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin COMMENT='Users and their assignments to user groups';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pma__users`
--

LOCK TABLES `pma__users` WRITE;
/*!40000 ALTER TABLE `pma__users` DISABLE KEYS */;
/*!40000 ALTER TABLE `pma__users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-29 10:26:07
