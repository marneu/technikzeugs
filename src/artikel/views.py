from artikel.serializers import UserSerializer
from django.contrib.auth.models import User
from artikel.permissions import IsOwnerOrReadOnly
from rest_framework import permissions
from rest_framework import generics
from artikel.serializers import ArtikelSerializer
from django.views import generic
from django.db.models import Q


from .models import Artikel


class IndexView(generic.ListView):
    template_name = "artikel/index.html"
    context_object_name = "latest_artikel_list"

    def get_queryset(self):
        """Return the last fifty published articles."""
        return Artikel.objects.order_by("-datum")[:50]


class DetailView(generic.DetailView):
    model = Artikel
    template_name = "artikel/detail.html"


class SearchView(generic.ListView):
    template_name = "artikel/search.html"
    context_object_name = "artikel_list"

    def get_queryset(self):
        query = self.request.GET.get("q")
        if query:
            # query_list = query.split()
            result = Artikel.objects.filter(
                Q(tags__icontains=query)
                | Q(titel__icontains=query)
                | Q(text__icontains=query)
            ).order_by("-datum")
        else:
            result = Artikel.objects.all().order_by("-datum")[:25]
        return result


# ------------------------ drf -------------------------------------------------


class ArtikelList(generics.ListCreateAPIView):
    queryset = Artikel.objects.all()
    serializer_class = ArtikelSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class ArtikelDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly,
        IsOwnerOrReadOnly,
    )
    queryset = Artikel.objects.all()
    serializer_class = ArtikelSerializer


# ------------------------ user ------------------------------------------------


class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
