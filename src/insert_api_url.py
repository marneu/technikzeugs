from os import environ
import sys
import fileinput
# from pathlib import Path

api_url = environ["API_URL"]
api_line = f"const api_url = '{api_url}'\n"
relative_file = "artikel/static/artikel/search.js"

# file = Path(relative_file).absolute()
# print(file)

# for docker
file = "/technikzeugs/" + relative_file

# with open(file, "r") as temp:
#    for line in temp.readlines():
#        "const api_url" in line and print(line)

for line in fileinput.input(file, inplace=1):
    if "const api_url" in line:
        line = api_line
    sys.stdout.write(line)
