# Technikzeugs

Technikzeugs is a tech-related weblog for personal use that I use to document various useful bits of information. 

Live demo [_here_](https://marneu.net/technikzeug/artikel/)

## Table of Contents
* [Technologies Used](#technologies-used)
* [Features](#features)
* [Setup](#setup)
* [Usage](#usage)
* [Project Status](#project-status)
* [Room for Improvement](#room-for-improvement)
* [Acknowledgements](#acknowledgements)

## Technologies Used  

- HTML+CSS
- SASS
- JavaScript (frontend only)
- Django 3.2
- Django REST framework 3.12
- uWSGI 2.0
- Python 3.9
- MariaDB 10.6
- Docker 20.10
- Nginx 1.18
- Debian 11


## Features  
- create, read, update, delete articles
- user authentication
- open articles inline, without going to the details page


## Screenshots 

landing page
![home](/screenshots/2021-09-26-150624_1011x737_scrot.png?raw=true "landing page")

django admin panel
![admin](/screenshots/2021-09-26-150518_1096x921_scrot.png?raw=true "django admin panel")


## Setup  

**setup with docker and marneu/technikzeugs:dev on remote host**
```sh
# on local host
# (substitute your values for USER, HOST, IDENTIY_FILE)
ssh USER@HOST -i ~/.ssh/IDENTIY_FILE

# in remote instance
sh install.sh
# test docker
sudo docker run hello-world
git clone git@bitbucket.org:marneu/technikzeugs.git
cd technikzeugs
micro .env # copy information here, replace API_URL
# initialize docker volumes
sudo docker-compose -f docker-compose.yml up

# back on local host
scp -i ~/.ssh/IDENTIY_FILE demo-database.sql \
    USER@HOST:/home/debian/technikzeugs/

# in remote instance
sudo cp demo-database.sql /var/lib/docker/volumes/technikzeugs_mariadb_data/_data/
sudo docker exec -it technikzeugs_database_1 bash
cd /var/lib/mysql
mysql -u root -p technikzeugs < demo-database.sql
exit
sudo cp nginx_docker.conf /etc/nginx/sites-enabled/HOST.conf
sudo rm /etc/nginx/sites-enabled/default
sudo service nginx reload
w3m localhost/artikel # should show technikzeugs landing page with article list

# test on local host
firefox HOST/artikel
```

## Usage 

```sh
# main frontend, view content here
firefox HOST/technikzeug/artikel

# admin panel (changes to content are made here)
# user: marneu, password: asfq09euh0
firefox HOST/technikzeug/admin

# browse django rest framework's API-explorer
firefox HOST/technikzeug/api
```


## Project Status  
Project is: _in progress_ 


## Room for Improvement  

Room for improvement:

- security settings in database, django config, uwsgi config
- accessibility

To do:

- set up sample database for demo
- set up live demo
- more detailed search form
- code highlighting in articles
- write and link guide for development workflow
- write tests


## Acknowledgements  

- readme template https://github.com/ritaly/README-cheatsheet 
